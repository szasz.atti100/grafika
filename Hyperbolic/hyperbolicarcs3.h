#pragma once

#include "../Core/LinearCombination3.h"

namespace cagd {

class HyperbolicArc3: public LinearCombination3
{
protected:
    GLdouble        _alpha;
    GLdouble        _alpha_constant1;
    GLdouble        _alpha_constant2;
    GLdouble        _alpha_constant3;

public:
    HyperbolicArc3(GLdouble alpha = 1.0, GLenum data_usage_flag = GL_STATIC_DRAW);
    GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
    GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const;

    GLboolean setAlpha(GLdouble alpha);
    GLdouble getAlpha();

private:
    void setConstants();

    //blending functions
    GLdouble blendingFunction0(GLdouble t) const;
    GLdouble blendingFunction1(GLdouble t) const;
    GLdouble blendingFunction2(GLdouble t) const;
    GLdouble blendingFunction3(GLdouble t) const;

    //first order derivatives
    GLdouble blendingFunction0_1(GLdouble t) const;
    GLdouble blendingFunction1_1(GLdouble t) const;
    GLdouble blendingFunction2_1(GLdouble t) const;
    GLdouble blendingFunction3_1(GLdouble t) const;

    //second order derivatives
    GLdouble blendingFunction0_2(GLdouble t) const;
    GLdouble blendingFunction1_2(GLdouble t) const;
    GLdouble blendingFunction2_2(GLdouble t) const;
    GLdouble blendingFunction3_2(GLdouble t) const;
};

}
