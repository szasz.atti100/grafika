#include "HyperbolicCompositeSurfaces3.h"
#include <fstream>

using namespace std;
namespace cagd {
HyperbolicCompositeSurface3::PatchAttributes::PatchAttributes()
{
    _patch = nullptr;
    _images = nullptr;
    _material = nullptr;
    _shader = nullptr;
    _u_lines = nullptr;
    _v_lines = nullptr;
    for (GLint i = 0; i < 8; i++) {
        _neighbors[i] = nullptr;
    }
}

HyperbolicCompositeSurface3::PatchAttributes::PatchAttributes(const PatchAttributes &rhs) :
    _patch(rhs._patch ? new SecondOrderHyperbolic(*rhs._patch) : nullptr),
    _images(rhs._images ? new TriangulatedMesh3(*rhs._images) : nullptr),
    _material(rhs._material),
    _shader(rhs._shader),
    _u_lines(rhs._u_lines ? new RowMatrix<GenericCurve3*>(rhs._u_lines->GetColumnCount()) : nullptr),
    _v_lines(rhs._v_lines ? new RowMatrix<GenericCurve3*>(rhs._v_lines->GetColumnCount()) : nullptr)
{
    // deep copy of u-directional isoparametric lines
    if (rhs._u_lines)
    {
        for (GLuint i = 0; i < rhs._u_lines->GetColumnCount(); i++)
        {
            if ((*rhs._u_lines)[i])
            {
                (*_u_lines)[i] = new GenericCurve3(*(*rhs._u_lines)[i]);
            }
        }
    }

    // deep copy of v-directional isoparametric lines
    if (rhs._v_lines)
    {
        for (GLuint j = 0; j < rhs._v_lines->GetColumnCount(); j++)
        {
            if ((*rhs._v_lines)[j])
            {
                (*_v_lines)[j] = new GenericCurve3(*(*rhs._u_lines)[j]);
            }
        }
    }

    for (GLuint i = 0; i < 8; i++) {
        _neighbors[i] = rhs._neighbors[i];
    }
}

HyperbolicCompositeSurface3::PatchAttributes& HyperbolicCompositeSurface3::PatchAttributes::operator =(const PatchAttributes& rhs)
{
    if (this != &rhs) {
        if(_patch)
            delete _patch, _patch = nullptr;
        if(_images)
            delete _images, _images = nullptr;

        if (_u_lines)
        {
            for (GLuint i = 0; i < _u_lines->GetColumnCount(); i++)
            {
                if ((*_u_lines)[i])
                {
                    delete (*_u_lines)[i], (*_u_lines)[i] = nullptr;
                }
            }
            delete _u_lines, _u_lines = nullptr;
        }


        if (_v_lines)
        {
            for (GLuint j = 0; j < _v_lines->GetColumnCount(); j++)
            {
                if ((*_v_lines)[j])
                {
                    delete (*_v_lines)[j], (*_v_lines)[j] = nullptr;
                }
            }
            delete _v_lines, _v_lines = nullptr;
        }


        _patch  = rhs._patch ? new SecondOrderHyperbolic(*rhs._patch) : nullptr;
        _images= rhs._images ? new TriangulatedMesh3(*rhs._images) : nullptr;
        _material = rhs._material;
        _shader = rhs._shader;

        _u_lines = new RowMatrix<GenericCurve3*>(rhs._u_lines->GetColumnCount());
        _v_lines = new RowMatrix<GenericCurve3*>(rhs._v_lines->GetColumnCount());

        if (rhs._u_lines)
        {
            for (GLuint i = 0; i < rhs._u_lines->GetColumnCount(); i++)
            {
                if ((*rhs._u_lines)[i])
                {
                    (*_u_lines)[i] = new GenericCurve3(*(*rhs._u_lines)[i]);
                }
            }
        }

        // deep copy of v-directional isoparametric lines
        if (rhs._v_lines)
        {
            for (GLuint j = 0; j < rhs._v_lines->GetColumnCount(); j++)
            {
                if ((*rhs._v_lines)[j])
                {
                    (*_v_lines)[j] = new GenericCurve3(*(*rhs._u_lines)[j]);
                }
            }
        }

        for (GLint i = 0; i < 8; i++) {
            _neighbors[i] = rhs._neighbors[i];
        }
    }
    return *this;
}

void HyperbolicCompositeSurface3::PatchAttributes::setPatch(SecondOrderHyperbolic *patch)
{
    _patch = patch;
}
void HyperbolicCompositeSurface3::PatchAttributes::setMaterial(Material &material)
{
    _material = &material;
}

HyperbolicCompositeSurface3::PatchAttributes::~PatchAttributes()
{
    if(_patch)
        delete _patch, _patch = nullptr;
    if(_images)
        delete _images, _images = nullptr;

    if (_u_lines)
    {
        for (GLuint i = 0; i < _u_lines->GetColumnCount(); i++)
        {
            if ((*_u_lines)[i])
            {
                delete (*_u_lines)[i], (*_u_lines)[i] = nullptr;
            }
        }
        delete _u_lines, _u_lines = nullptr;
    }

    if (_v_lines)
    {
        for (GLuint j = 0; j < _v_lines->GetColumnCount(); j++)
        {
            if ((*_v_lines)[j])
            {
                delete (*_v_lines)[j], (*_v_lines)[j] = nullptr;
            }
        }
        delete _u_lines, _u_lines = nullptr;
    }
}

HyperbolicCompositeSurface3::HyperbolicCompositeSurface3(GLdouble alpha, GLuint maxPatchCapacity) {
    _alpha = alpha;
    _maxPatchCapacity = maxPatchCapacity;
    _attributes.reserve(maxPatchCapacity);
}

HyperbolicCompositeSurface3::HyperbolicCompositeSurface3(const HyperbolicCompositeSurface3 &rhs) :
    _alpha(rhs._alpha),
    _maxPatchCapacity(rhs._maxPatchCapacity),
    _attributes(rhs._attributes)
{}

HyperbolicCompositeSurface3::~HyperbolicCompositeSurface3() {
    _attributes.clear();
}

HyperbolicCompositeSurface3& HyperbolicCompositeSurface3::operator =(const HyperbolicCompositeSurface3 &rhs) {
    if (this != &rhs) {
        _alpha = rhs._alpha;
        _maxPatchCapacity = rhs._maxPatchCapacity;
        _attributes = rhs._attributes;
    }

    return *this;
}

void HyperbolicCompositeSurface3::insertPatch(PatchAttributes attr)
{
    _attributes.push_back(attr);
}

HyperbolicCompositeSurface3::PatchAttributes& HyperbolicCompositeSurface3::getAtIndex(GLuint index)
{
//    if(&_attributes[index]) cout << "letezik" << endl;
    return _attributes[index];
}


GLboolean HyperbolicCompositeSurface3::insertNewIsolatedDefaultPatch(
        GLuint u_div_point_count, GLuint v_div_point_count,
        GLuint u_iso_line_count, GLuint v_iso_line_count,
        GLenum usage_flag)
{
    vector<PatchAttributes> *initial_address = &_attributes;

    GLuint n = (GLuint)_attributes.size();

    _attributes.resize(n + 1);
    _attributes[n]._patch = new (nothrow) SecondOrderHyperbolic(_alpha);

    if (!_attributes[n]._patch)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    // TO DO: set the default position of control points
    _attributes[n]._patch->SetData(0, 0, -2.0, -2.0, 0.0);
    _attributes[n]._patch->SetData(0, 1, -2.0, -1.0, 0.0);
    _attributes[n]._patch->SetData(0, 2, -2.0, 1.0, 0.0);
    _attributes[n]._patch->SetData(0, 3, -2.0, 2.0, 3.0);

    _attributes[n]._patch->SetData(1, 0, -1.0, -2.0, 0.0);
    _attributes[n]._patch->SetData(1, 1, -1.0, -1.0, 2.0);
    _attributes[n]._patch->SetData(1, 2, -1.0, 1.0, 2.0);
    _attributes[n]._patch->SetData(1, 3, -1.0, 2.0, 0.0);

    _attributes[n]._patch->SetData(2, 0, 1.0, -2.0, 0.0);
    _attributes[n]._patch->SetData(2, 1, 1.0, -1.0, 2.0);
    _attributes[n]._patch->SetData(2, 2, 1.0, 1.0, 2.0);
    _attributes[n]._patch->SetData(2, 3, 1.0, 2.0, 0.0);

    _attributes[n]._patch->SetData(3, 0, 2.0, -2.0, 3.0);
    _attributes[n]._patch->SetData(3, 1, 2.0, -1.0, 0.0);
    _attributes[n]._patch->SetData(3, 2, 2.0, 1.0, 0.0);
    _attributes[n]._patch->SetData(3, 3, 2.0, 2.0, 0.0);

    _attributes[n]._patch->UpdateVertexBufferObjectsOfData();

    _attributes[n]._images = _attributes[n]._patch->GenerateImage(
                u_div_point_count, v_div_point_count, usage_flag);

    if (!_attributes[n]._images)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    if (!_attributes[n]._images->UpdateVertexBufferObjects(usage_flag))
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    _attributes[n]._u_lines = _attributes[n]._patch->GenerateUIsoparametricLines(
                u_iso_line_count, 1, u_div_point_count, usage_flag);

    if (!_attributes[n]._u_lines)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    for (GLuint i = 0; i < _attributes[n]._u_lines->GetColumnCount(); i++)
    {
        if (!(*_attributes[n]._u_lines)[i]->UpdateVertexBufferObjects(usage_flag))
        {
            _attributes.pop_back();
            return GL_FALSE;
        }
    }

    _attributes[n]._v_lines = _attributes[n]._patch->GenerateVIsoparametricLines(
                v_iso_line_count, 1, v_div_point_count, usage_flag);

    if (!_attributes[n]._v_lines)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    for (GLuint j = 0; j < _attributes[n]._v_lines->GetColumnCount(); j++)
    {
        if (!(*_attributes[n]._v_lines)[j]->UpdateVertexBufferObjects(usage_flag))
        {
            _attributes.pop_back();
            return GL_FALSE;
        }
    }

    // TO DO: set the default material and shader pointers (which should reference
    // existing predefined material and shader objects, respectively)

    if (n + 1 > _maxPatchCapacity)
    {
        vector<PatchAttributes> *new_address = &_attributes;
        size_t offset = new_address - initial_address;

        // TO DO: update the neighbor pointers based on the offset parameter
    }

    return GL_TRUE;
}

void HyperbolicCompositeSurface3::updateNeighborsAfterTransform(PatchAttributes &attr,
                                                                std::vector<PatchAttributes*> alreadyUpdated,
                                                                GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ)
{
    GLdouble x, y, z;

    for(GLuint i = 0; i < 4; i++)
    {
        for(GLuint j = 0; j < 4; j++)
        {
             attr._patch->GetData(i, j, x, y, z);
                 x += shiftX;
                 y += shiftY;
                 z += shiftZ;
             attr._patch->SetData(i, j, x, y, z);
        }
    }

    attr._patch->UpdateVertexBufferObjectsOfData();

    GLuint index;
    for(index = 0; index < _attributes.size(); index++)
    {
        if(&_attributes[index] == &attr)
            break;
    }
    updateAfterPatchChanged(index);

    alreadyUpdated.push_back(&attr);

    for(GLuint i = 0; i < 8; i++)
    {
        GLboolean ok = GL_TRUE;
        if(attr._neighbors[i])
        {
            for(GLuint j = 0; j < alreadyUpdated.size(); j++)
            {
                if(alreadyUpdated[j] == attr._neighbors[i])
                {
                    ok = GL_FALSE;
                    break;
                }
            }
            if(ok)
            {
                updateNeighborsAfterTransform((*attr._neighbors[i]), alreadyUpdated, shiftX, shiftY, shiftZ);
            }
        }
    }
}

void HyperbolicCompositeSurface3::transformPatch(GLuint index, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ)
{
    GLdouble x, y, z;

    for(GLuint i = 0; i < 4; i++)
    {
        for(GLuint j = 0; j < 4; j++)
        {
             _attributes[index]._patch->GetData(i, j, x, y, z);
                 x += shiftX;
                 y += shiftY;
                 z += shiftZ;
             _attributes[index]._patch->SetData(i, j, x, y, z);
        }
    }

    _attributes[index]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(index);

    std::vector<PatchAttributes*> alreadyUpdated;

    alreadyUpdated.push_back(&_attributes[index]);

    for(GLuint i = 0; i < 8; i++)
    {
        GLboolean ok = GL_TRUE;
        if(_attributes[index]._neighbors[i])
        {
            for(GLuint j = 0; j < alreadyUpdated.size(); j++)
            {
                if(alreadyUpdated[j] == _attributes[index]._neighbors[i])
                {
                    ok = GL_FALSE;
                    break;
                }
            }
            if(ok)
            {
                updateNeighborsAfterTransform((*_attributes[index]._neighbors[i]), alreadyUpdated, shiftX, shiftY, shiftZ);
            }
        }
    }
}

void HyperbolicCompositeSurface3::updateAfterPatchChanged(GLuint index, GLuint u_div_point_count, GLuint v_div_point_count)
{
    if(_attributes[index]._images)
        delete _attributes[index]._images;
    _attributes[index]._images = _attributes[index]._patch->GenerateImage(u_div_point_count, v_div_point_count);

    if (!_attributes[index]._images)
    {
        throw ("could not create image - hyperbolicCompositeSurface");
    }

    if (!_attributes[index]._images->UpdateVertexBufferObjects())
    {
        throw ("could not update imageVBO - hyperbolicCompositeSurface");
    }


    if (_attributes[index]._u_lines)
    {
        for (GLuint i = 0; i < _attributes[index]._u_lines->GetColumnCount(); i++)
        {
            if ((*_attributes[index]._u_lines)[i])
            {
                delete (*_attributes[index]._u_lines)[i], (*_attributes[index]._u_lines)[i] = nullptr;
            }
        }
    }
    delete _attributes[index]._u_lines, _attributes[index]._u_lines = nullptr;

    if (_attributes[index]._v_lines)
    {
        for (GLuint j = 0; j < _attributes[index]._v_lines->GetColumnCount(); j++)
        {
            if ((*_attributes[index]._v_lines)[j])
            {
                delete (*_attributes[index]._v_lines)[j], (*_attributes[index]._v_lines)[j] = nullptr;
            }
        }
    }
    delete _attributes[index]._u_lines, _attributes[index]._u_lines = nullptr;


    _attributes[index]._u_lines = _attributes[index]._patch->GenerateUIsoparametricLines(2, 1, 30, 35044);

    if (!_attributes[index]._u_lines)
    {
        throw ("could not update u_lines - hyperbolicCompositeSurface");
    }

    for (GLuint i = 0; i < _attributes[index]._u_lines->GetColumnCount(); i++)
    {
        if (!(*_attributes[index]._u_lines)[i]->UpdateVertexBufferObjects())
        {
            throw ("could not update u_line[i] - hyperbolicCompositeSurface");
        }
    }

    _attributes[index]._v_lines = _attributes[index]._patch->GenerateVIsoparametricLines(2, 1, 30, 35044);

    if (!_attributes[index]._v_lines)
    {
        throw ("could not update v_lines - hyperbolicCompositeSurface");
    }

    for (GLuint j = 0; j < _attributes[index]._v_lines->GetColumnCount(); j++)
    {
        if (!(*_attributes[index]._v_lines)[j]->UpdateVertexBufferObjects())
        {
            throw ("could not update v_lines[i] - hyperbolicCompositeSurface");
        }
    }
}

GLboolean HyperbolicCompositeSurface3::joinExistingPatches(GLuint firstIndex, GLuint secondIndex,
                                                           Direction firstDirection, Direction secondDirection)
{
    if(firstIndex == secondIndex)
        return GL_FALSE;

    if(_attributes[firstIndex]._neighbors[firstDirection])
        return GL_FALSE;
    if(_attributes[secondIndex]._neighbors[secondDirection])
        return GL_FALSE;


    //vector<PatchAttributes> *initial_address = &_attributes;

    GLuint n = (GLuint)_attributes.size();

    _attributes.resize(n + 1);
    _attributes[n]._patch = new (nothrow) SecondOrderHyperbolic(_alpha);

    if (!_attributes[n]._patch)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }


    for(GLuint i = 0; i < 4; i++)
    {
        switch(firstDirection){
        case EAST:
            _attributes[n]._patch->SetData(0, i, (*_attributes[firstIndex]._patch)(3, i));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[firstIndex]._patch)(3, i) - (*_attributes[firstIndex]._patch)(2, i));
            _attributes[firstIndex]._neighbors[EAST] = &_attributes[n];
            break;
        case WEST:
            _attributes[n]._patch->SetData(0, i, (*_attributes[firstIndex]._patch)(0, i));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[firstIndex]._patch)(0, i) - (*_attributes[firstIndex]._patch)(1, i));
            _attributes[firstIndex]._neighbors[WEST] = &_attributes[n];
            break;
        case NORTH:
            _attributes[n]._patch->SetData(0, i, (*_attributes[firstIndex]._patch)(i, 3));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[firstIndex]._patch)(i, 3) - (*_attributes[firstIndex]._patch)(i, 2));
            _attributes[firstIndex]._neighbors[NORTH] = &_attributes[n];
            break;
        case SOUTH:
            _attributes[n]._patch->SetData(0, i, (*_attributes[firstIndex]._patch)(i, 0));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[firstIndex]._patch)(i, 0) - (*_attributes[firstIndex]._patch)(i, 1));
            _attributes[firstIndex]._neighbors[SOUTH] = &_attributes[n];
            break;
        }
    }

    for(GLuint i = 0; i < 4; i++)
    {
        switch(secondDirection){
        case WEST:
            _attributes[n]._patch->SetData(3, i, (*_attributes[secondIndex]._patch)(0, i));
            _attributes[n]._patch->SetData(2, i, 2.0 * (*_attributes[secondIndex]._patch)(0, i) - (*_attributes[secondIndex]._patch)(1, i));
            _attributes[secondIndex]._neighbors[WEST] = &_attributes[n];
            break;
        case EAST:
            _attributes[n]._patch->SetData(3, i, (*_attributes[secondIndex]._patch)(3, i));
            _attributes[n]._patch->SetData(2, i, 2.0 * (*_attributes[secondIndex]._patch)(3, i) - (*_attributes[secondIndex]._patch)(2, i));
            _attributes[secondIndex]._neighbors[EAST] = &_attributes[n];
            break;
        case NORTH:
            _attributes[n]._patch->SetData(3, i, (*_attributes[secondIndex]._patch)(i, 3));
            _attributes[n]._patch->SetData(2, i, 2.0 * (*_attributes[secondIndex]._patch)(i, 3) - (*_attributes[secondIndex]._patch)(i, 2));
            _attributes[secondIndex]._neighbors[NORTH] = &_attributes[n];
            break;
        case SOUTH:
            _attributes[n]._patch->SetData(3, i, (*_attributes[secondIndex]._patch)(i, 0));
            _attributes[n]._patch->SetData(2, i, 2.0 * (*_attributes[secondIndex]._patch)(i, 0) - (*_attributes[secondIndex]._patch)(i, 1));
            _attributes[secondIndex]._neighbors[SOUTH] = &_attributes[n];
            break;
        }
    }

    _attributes[n]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(n);
    _attributes[n]._neighbors[WEST] = &_attributes[firstIndex];
    _attributes[n]._neighbors[EAST] = &_attributes[secondIndex];
    return GL_TRUE;

}


GLboolean HyperbolicCompositeSurface3::continueExistingPatch(GLuint index, Direction direction)
{
    if(_attributes[index]._neighbors[direction])
        return GL_FALSE;

    //vector<PatchAttributes> *initial_address = &_attributes;

    GLuint n = (GLuint)_attributes.size();

    _attributes.resize(n + 1);
    _attributes[n]._patch = new (nothrow) SecondOrderHyperbolic(_alpha);

    if (!_attributes[n]._patch)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    if(direction == EAST)
    {
        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[n]._patch->SetData(0, i, (*_attributes[index]._patch)(3, i));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[index]._patch)(3, i) - (*_attributes[index]._patch)(2, i));
            _attributes[n]._patch->SetData(2, i, 3.0 * (*_attributes[index]._patch)(3, i) - 2.0 * (*_attributes[index]._patch)(2, i));
            _attributes[n]._patch->SetData(3, i, 4.0 * (*_attributes[index]._patch)(3, i) - 3.0 * (*_attributes[index]._patch)(2, i));
        }
    }

    if(direction == WEST)
    {
        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[n]._patch->SetData(0, i, (*_attributes[index]._patch)(0, 3 - i));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[index]._patch)(0, 3 - i) - (*_attributes[index]._patch)(1, 3 - i));
            _attributes[n]._patch->SetData(2, i, 3.0 * (*_attributes[index]._patch)(0, 3 - i) - 2.0 * (*_attributes[index]._patch)(1, 3 - i));
            _attributes[n]._patch->SetData(3, i, 4.0 * (*_attributes[index]._patch)(0, 3 - i) - 3.0 * (*_attributes[index]._patch)(1, 3 - i));
        }
    }

    if(direction == SOUTH)
    {
        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[n]._patch->SetData(0, i, (*_attributes[index]._patch)(i, 0));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[index]._patch)(i, 0) - (*_attributes[index]._patch)(i, 1));
            _attributes[n]._patch->SetData(2, i, 3.0 * (*_attributes[index]._patch)(i, 0) - 2.0 * (*_attributes[index]._patch)(i, 1));
            _attributes[n]._patch->SetData(3, i, 4.0 * (*_attributes[index]._patch)(i, 0) - 3.0 * (*_attributes[index]._patch)(i, 1));
        }
    }

    if(direction == NORTH)
    {
        for(GLuint i = 0; i < 4; i++)
        {
            _attributes[n]._patch->SetData(0, i, (*_attributes[index]._patch)(3 - i, 3));
            _attributes[n]._patch->SetData(1, i, 2.0 * (*_attributes[index]._patch)(3 - i, 3) - (*_attributes[index]._patch)(3 - i, 2));
            _attributes[n]._patch->SetData(2, i, 3.0 * (*_attributes[index]._patch)(3 - i, 3) - 2.0 * (*_attributes[index]._patch)(3 - i, 2));
            _attributes[n]._patch->SetData(3, i, 4.0 * (*_attributes[index]._patch)(3 - i, 3) - 3.0 * (*_attributes[index]._patch)(3 - i, 2));
        }
    }

    _attributes[n]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(n);

    _attributes[index]._neighbors[direction] = &_attributes[n];
    _attributes[n]._neighbors[WEST] = &_attributes[index];

    return GL_TRUE;
}

void HyperbolicCompositeSurface3::mergeExistingPatchesAfterMerge(GLuint firstIndex, GLuint secondIndex,
                                    Direction firstDirection, Direction secondDirection,
                                    std::vector<PatchAttributes*> alreadyChanged)
{
    RowMatrix<DCoordinate3> newPoints;
    newPoints.ResizeColumns(4);
    GLdouble x1, x2, y1, y2, z1, z2;
    GLuint firstPosition1, secondPosition1, firstPosition2, secondPosition2;

    for(GLuint i = 0; i < 4; i++)
    {
        switch(secondDirection)
        {
        case WEST:
            _attributes[secondIndex]._patch->GetData(1, i, x2, y2, z2);
            firstPosition1 = 1;
            secondPosition1 = i;
            firstPosition2 = 0;
            secondPosition2 = i;
            break;
        case EAST:
            _attributes[secondIndex]._patch->GetData(2, i, x2, y2, z2);
            firstPosition1 = 2;
            secondPosition1 = i;
            firstPosition2 = 3;
            secondPosition2 = i;
            break;
        case SOUTH:
            _attributes[secondIndex]._patch->GetData(i, 1, x2, y2, z2);
            firstPosition1 = i;
            secondPosition1 = 1;
            firstPosition2 = i;
            secondPosition2 = 0;
            break;
        case NORTH:
            _attributes[secondIndex]._patch->GetData(i, 2, x2, y2, z2);
            firstPosition1 = i;
            secondPosition1 = 2;
            firstPosition2 = i;
            secondPosition2 = 3;
            break;
        }

        switch(firstDirection)
        {
        case EAST:
            _attributes[firstIndex]._patch->GetData(3, i, x1, y1, z1);
            _attributes[secondIndex]._patch->SetData(firstPosition1, secondPosition1,
                                                     2.0 * (*_attributes[firstIndex]._patch)(3, i) - (*_attributes[firstIndex]._patch)(2, i));
            break;
        case WEST:
            _attributes[firstIndex]._patch->GetData(0, i, x1, y1, z1);
            _attributes[secondIndex]._patch->SetData(firstPosition1, secondPosition1,
                                                     2.0 * (*_attributes[firstIndex]._patch)(0, i) - (*_attributes[firstIndex]._patch)(1, i));
            break;
        case SOUTH:
            _attributes[firstIndex]._patch->GetData(i, 0, x1, y1, z1);
            _attributes[secondIndex]._patch->SetData(firstPosition1, secondPosition1,
                                                     2.0 * (*_attributes[firstIndex]._patch)(i, 0) - (*_attributes[firstIndex]._patch)(i, 1));
            break;
        case NORTH:
            _attributes[firstIndex]._patch->GetData(i, 3, x1, y1, z1);
            _attributes[secondIndex]._patch->SetData(firstPosition1, secondPosition1,
                                                     2.0 * (*_attributes[firstIndex]._patch)(i, 3) - (*_attributes[firstIndex]._patch)(i, 2));
            break;
        }

        _attributes[secondIndex]._patch->SetData(firstPosition2, secondPosition2, x1, y1, z1);
    }

    _attributes[secondIndex]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(secondIndex);

    alreadyChanged.push_back(&_attributes[secondIndex]);

    for(int i = 0; i < 8; i++)
    {
        if(_attributes[secondIndex]._neighbors[i])
        {
            bool ok = true;
            for(int k = 0; k < alreadyChanged.size(); k++)
            {
                if(alreadyChanged[k] == _attributes[secondIndex]._neighbors[i])
                {
                    ok = false;
                    break;
                }
            }

            if(ok)
            {
                GLuint index = findIndex((*_attributes[secondIndex]._neighbors[i]));
                int j;
                for(j = 0; j < 8; j++)
                {
                    if(_attributes[secondIndex]._neighbors[i]->_neighbors[j] == &_attributes[secondIndex])
                        break;
                }
                HyperbolicCompositeSurface3::Direction dir1 = HyperbolicCompositeSurface3::Direction(i);
                HyperbolicCompositeSurface3::Direction dir2 = HyperbolicCompositeSurface3::Direction(j);
                mergeExistingPatchesAfterMerge(secondIndex, index, dir1, dir2, alreadyChanged);
            }
        }
    }
}

GLboolean HyperbolicCompositeSurface3::mergeExistingPatches(GLuint firstIndex, GLuint secondIndex,
                                                       Direction firstDirection, Direction secondDirection)
{
    if (firstIndex == secondIndex)
        return GL_FALSE;
    //find the new point
    RowMatrix<DCoordinate3> newPoints;
    newPoints.ResizeColumns(4);
    GLdouble x1, x2, y1, y2, z1, z2;
    GLuint firstPosition1, secondPosition1, firstPosition2, secondPosition2;

    for(GLuint i = 0; i < 4; i++)
    {

        switch(firstDirection)
        {
        case EAST:
            _attributes[firstIndex]._patch->GetData(2, i, x1, y1, z1);
            firstPosition1 = 3;
            secondPosition1 = i;
            _attributes[firstIndex]._neighbors[EAST] = &_attributes[secondIndex];
            break;
        case WEST:
            _attributes[firstIndex]._patch->GetData(1, i, x1, y1, z1);
            firstPosition1 = 0;
            secondPosition1 = i;
            _attributes[firstIndex]._neighbors[WEST] = &_attributes[secondIndex];
            break;
        case SOUTH:
            _attributes[firstIndex]._patch->GetData(i, 1, x1, y1, z1);
            firstPosition1 = i;
            secondPosition1 = 0;
            _attributes[firstIndex]._neighbors[SOUTH] = &_attributes[secondIndex];
            break;
        case NORTH:
            _attributes[firstIndex]._patch->GetData(i, 2, x1, y1, z1);
            firstPosition1 = i;
            secondPosition1 = 3;
            _attributes[firstIndex]._neighbors[NORTH] = &_attributes[secondIndex];
            break;
        }

        switch(secondDirection)
        {
        case WEST:
            _attributes[secondIndex]._patch->GetData(1, i, x2, y2, z2);
            firstPosition2 = 0;
            secondPosition2 = i;
            _attributes[secondIndex]._neighbors[WEST] = &_attributes[firstIndex];
            break;
        case EAST:
            _attributes[secondIndex]._patch->GetData(2, i, x2, y2, z2);
            firstPosition2 = 3;
            secondPosition2 = i;
            _attributes[secondIndex]._neighbors[EAST] = &_attributes[firstIndex];
            break;
        case SOUTH:
            _attributes[secondIndex]._patch->GetData(i, 1, x2, y2, z2);
            firstPosition2 = i;
            secondPosition2 = 0;
            _attributes[secondIndex]._neighbors[SOUTH] = &_attributes[firstIndex];
            break;
        case NORTH:
            _attributes[secondIndex]._patch->GetData(i, 2, x2, y2, z2);
            firstPosition2 = i;
            secondPosition2 = 3;
            _attributes[secondIndex]._neighbors[NORTH] = &_attributes[firstIndex];
            break;
        }

        _attributes[firstIndex]._patch->SetData(firstPosition1, secondPosition1, (x1 + x2) / 2, (y1 + y2) / 2, (z1 + z2) / 2);
        _attributes[secondIndex]._patch->SetData(firstPosition2, secondPosition2, (x1 + x2) / 2, (y1 + y2) / 2, (z1 + z2) / 2);
    }


    _attributes[firstIndex]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(firstIndex);

    _attributes[secondIndex]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(secondIndex);

    std::vector<PatchAttributes*> alreadyChanged;
    alreadyChanged.push_back(&_attributes[firstIndex]);
    alreadyChanged.push_back(&_attributes[secondIndex]);

    for(int i = 0; i < 8; i++)
    {
        if(_attributes[firstIndex]._neighbors[i] && _attributes[firstIndex]._neighbors[i] != &_attributes[secondIndex])
        {
            GLuint index = findIndex((*_attributes[firstIndex]._neighbors[i]));
            int j;
            for(j = 0; j < 8; j++)
            {
                if(_attributes[firstIndex]._neighbors[i]->_neighbors[j] == &_attributes[firstIndex])
                    break;
            }
            HyperbolicCompositeSurface3::Direction dir1 = HyperbolicCompositeSurface3::Direction(i);
            HyperbolicCompositeSurface3::Direction dir2 = HyperbolicCompositeSurface3::Direction(j);
            mergeExistingPatchesAfterMerge(firstIndex, index, dir1, dir2, alreadyChanged);
        }
        if(_attributes[secondIndex]._neighbors[i] && _attributes[secondIndex]._neighbors[i] != &_attributes[firstIndex])
        {
            GLuint index = findIndex((*_attributes[secondIndex]._neighbors[i]));
            int j;
            for(j = 0; j < 8; j++)
            {
                if(_attributes[secondIndex]._neighbors[i]->_neighbors[j] == &_attributes[secondIndex])
                    break;
            }
            HyperbolicCompositeSurface3::Direction dir1 = HyperbolicCompositeSurface3::Direction(i);
            HyperbolicCompositeSurface3::Direction dir2 = HyperbolicCompositeSurface3::Direction(j);
            mergeExistingPatchesAfterMerge(secondIndex, index, dir1, dir2, alreadyChanged);
        }
    }

    return GL_TRUE;
}

GLuint HyperbolicCompositeSurface3::getAttributesSize()
{
    return _attributes.size();
}

void HyperbolicCompositeSurface3::modifyExistingPatch(GLuint index, GLuint x, GLuint y, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ)
{
    GLdouble initialX, initialZ, initialY;
    _attributes[index]._patch->GetData(x, y, initialX, initialY, initialZ);
    _attributes[index]._patch->SetData(x, y, initialX + shiftX, initialY + shiftY, initialZ + shiftZ);

    _attributes[index]._patch->UpdateVertexBufferObjectsOfData();

    updateAfterPatchChanged(index);

    std::vector<PatchAttributes*> alreadyChanged;
    alreadyChanged.push_back(&_attributes[index]);

    for(GLuint i = 0; i < 8; i++)
    {
        if(_attributes[index]._neighbors[i])
        {
            GLuint j;
            for(j = 0; j < 8; j++)
            {
                if(_attributes[index]._neighbors[i]->_neighbors[j] == &_attributes[index])
                    break;
            }
            mergeExistingPatchesAfterMerge(index, findIndex(*_attributes[index]._neighbors[i]),
                                           Direction(i), Direction(j), alreadyChanged);
        }
    }
}

void HyperbolicCompositeSurface3::eraseExistingPatch(GLuint index)
{
//    if(_attributes[index]._patch)
//        delete _attributes[index]._patch, _attributes[index]._patch = nullptr;
//    if(_attributes[index]._images)
//        delete _attributes[index]._images, _attributes[index]._images = nullptr;

//    if (_attributes[index]._u_lines)
//    {
//        for (GLuint i = 0; i < _attributes[index]._u_lines->GetColumnCount(); i++)
//        {
//            if ((*_attributes[index]._u_lines)[i])
//            {
//                delete (*_attributes[index]._u_lines)[i], (*_attributes[index]._u_lines)[i] = nullptr;
//            }
//        }
//    }
//    delete _attributes[index]._u_lines, _attributes[index]._u_lines = nullptr;

//    if (_attributes[index]._v_lines)
//    {
//        for (GLuint j = 0; j < _attributes[index]._v_lines->GetColumnCount(); j++)
//        {
//            if ((*_attributes[index]._v_lines)[j])
//            {
//                delete (*_attributes[index]._v_lines)[j], (*_attributes[index]._v_lines)[j] = nullptr;
//            }
//        }
//    }
//    delete _attributes[index]._u_lines, _attributes[index]._u_lines = nullptr;

//    for(GLuint i = 0; i < 8; i++)
//    {
//        if(_attributes[index]._neighbors[i])
//        {
//            for(GLuint j = 0; j < 8; j++)
//            {
//                if(_attributes[index]._neighbors[i]->_neighbors[j] == &_attributes[index])
//                    _attributes[index]._neighbors[i]->_neighbors[j] == nullptr;
//            }
//        }
//    }

    cout << "na itt" << endl;
    GLuint n = _attributes.size() - 1;
    cout << "n: " << n << endl;
    PatchAttributes *end_address = &_attributes[n];
    PatchAttributes *index_address = &_attributes[index];

    cout << "meg nincs hiba" << endl;
    for(GLuint i = 0; i <= n; i++)
    {
        for(GLuint j = 0; j < 8; j++)
        {
            if(_attributes[i]._neighbors[j] == end_address)
                _attributes[i]._neighbors[j] = index_address;
        }
    }

    if(index == n){
        _attributes.pop_back();
        return;
    }

    cout << "vege a szomszedok frissitesenek" << endl;
    PatchAttributes temp(_attributes[n]);
    cout << "esldo" << endl;
    _attributes[n] = _attributes[index];
    cout << "masodik" << endl;
    _attributes[index] = temp;
    cout << "harnadik" <<endl;
    _attributes.pop_back();

    cout << "popback utan" << endl;
    _attributes[index]._patch->UpdateVertexBufferObjectsOfData();
    updateAfterPatchChanged(index);

    std::cout << "index" << index << std::endl;
    std::cout << "size " << n + 1 <<std::endl;

}

void HyperbolicCompositeSurface3::renderPatchImages()
{
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        if(_attributes[i]._material)
            _attributes[i]._material->Apply();
        _attributes[i]._images->Render();

    }
}

void HyperbolicCompositeSurface3::renderNormalVectors()
{
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._images->RenderNormals();

    }
}

void HyperbolicCompositeSurface3::renderUVLines()
{
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        if((*_attributes[i]._u_lines)[0])
        {
            glColor3f(1.0f, 0.0f, 0.0f);
            (*_attributes[i]._u_lines)[0]->RenderDerivatives(0, GL_LINE_STRIP);
        }
        if((*_attributes[i]._u_lines)[1])
        {
            glColor3f(0.0f, 1.0f, 0.0f);
            (*_attributes[i]._u_lines)[1]->RenderDerivatives(0, GL_LINE_STRIP);
        }

        if((*_attributes[i]._v_lines)[0])
        {
            glColor3f(0.0f, 0.0f, 1.0f);
            (*_attributes[i]._v_lines)[0]->RenderDerivatives(0, GL_LINE_STRIP);
        }
        if((*_attributes[i]._v_lines)[1])
        {
            glColor3f(1.0f, 1.0f, 0.0f);
            (*_attributes[i]._v_lines)[1]->RenderDerivatives(0, GL_LINE_STRIP);
        }
    }
}

void HyperbolicCompositeSurface3::renderUVLinesDerivatives()
{
    glColor3f(1.0f, 1.0f, 0.0f);

    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        for(GLuint j = 0; j < _attributes[i]._u_lines->GetColumnCount(); j++)
        {
            if((*_attributes[i]._u_lines)[j])
            {
                (*_attributes[i]._u_lines)[j]->RenderDerivatives(1, GL_LINES);
            }
        }
    }

    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        for(GLuint j = 0; j < _attributes[i]._v_lines->GetColumnCount(); j++)
        {
            if((*_attributes[i]._v_lines)[j])
            {
                (*_attributes[i]._v_lines)[j]->RenderDerivatives(1, GL_LINES);
            }
        }
    }
}

void HyperbolicCompositeSurface3::renderPatch()
{
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._patch->RenderData();
    }
}

void HyperbolicCompositeSurface3::renderPatchColored(GLuint index)
{

//    _attributes[index]._patch->RenderDataColored();

}


GLuint HyperbolicCompositeSurface3::findIndex(const PatchAttributes &attr)
{
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        if(&_attributes[i] == &attr)
            return i;
    }
}

GLboolean HyperbolicCompositeSurface3::setAlpha(GLdouble alpha)
{
    _alpha = alpha;
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._patch->set_alpha(_alpha);
        updateAfterPatchChanged(i);
    }

    return GL_TRUE;
}

GLboolean HyperbolicCompositeSurface3::print(std::string filname)
{
    std::fstream f;
    f.open(filname, std::ios::out);
    f << _alpha << std::endl;
    f << _attributes.size() << std::endl;

    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        for(GLuint row = 0; row < 4; row++)
        {
            for(GLuint column = 0; column < 4; column++)
            {
                f << (*_attributes[i]._patch)(row, column) << " ";
            }
        }
        f << std::endl;
    }

    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        for(GLuint j = 0; j < 8; j++)
        {
            if(_attributes[i]._neighbors[j])
                f << findIndex(*_attributes[i]._neighbors[j]) << " ";
            else
                f << -1 << " ";
        }
        f << std::endl;
    }

    f.close();
    return GL_TRUE;
}

GLboolean HyperbolicCompositeSurface3::read(std::string filename)
{
    fstream f;
    f.open(filename, ios::in);
    if (!f.good()) return GL_FALSE;
    GLuint size;
    f >> _alpha;
    f >> size;

    for(GLuint i = 0; i < size; i++)
    {
        GLuint n = (GLuint)_attributes.size();

        _attributes.resize(n + 1);
        _attributes[n]._patch = new (nothrow) SecondOrderHyperbolic(_alpha);

        if (!_attributes[n]._patch)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }

        for(GLuint row = 0; row < 4; row++)
        {
            for(GLuint column = 0; column < 4; column++)
            {
                GLdouble x, y, z;
                f >> x >>y >> z;
                _attributes[n]._patch->SetData(row, column, x, y, z);
            }
        }

        _attributes[n]._patch->UpdateVertexBufferObjectsOfData();

        updateAfterPatchChanged(n);
    }


    for(GLuint i = 0; i< size; i++)
    {
        for(GLuint j = 0; j < 8; j++)
        {
            int index;
            f >> index;

            if(index < 0)
            {
                _attributes[i]._neighbors[j] = nullptr;
            } else
            {
                _attributes[i]._neighbors[j] = &_attributes[index];
            }
        }
    }

    f.close();
    return GL_TRUE;
}
void HyperbolicCompositeSurface3::setMaterial(GLuint index, Material &material)
{
    _attributes[index].setMaterial(material);
}

GLdouble HyperbolicCompositeSurface3::getAlpha()
{
    return _alpha;
}

}
