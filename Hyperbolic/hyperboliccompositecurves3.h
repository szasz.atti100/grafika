#pragma once
#pragma once
#include "../Hyperbolic/hyperbolicarcs3.h"
#include "../Core/GenericCurves3.h"
#include "../Core/Colors4.h"
#include <string>


namespace cagd {

class HyperbolicCompositeCurve3
{
public:
    enum Direction {LEFT = 0, RIGHT = 1};
    class ArcAttributes
    {
    public:
        HyperbolicArc3*     _arc;
        GenericCurve3*      _arc_image;
        Color4              *_color; //combobox akar a szinekkel
        ArcAttributes       *_next, *_previous;

        ArcAttributes();
        ArcAttributes(const ArcAttributes &rhs);
        ArcAttributes& operator = (const ArcAttributes& rhs);
        ~ArcAttributes();
        //constructor, copy constructor, operator =,destruktor
        //setterek/getters
    };

protected:
    std::vector<ArcAttributes>  _attributes;
    GLdouble                    _alpha;
    GLuint                      _maxArcCapacity;

public:
    HyperbolicCompositeCurve3(GLdouble alpha, GLuint max_arc_count = 1000); // csak a vektor reserve metodusat hivja meg, nem vegez helyfoglalast!!!
    HyperbolicCompositeCurve3(const HyperbolicCompositeCurve3 &rhs);
    ~HyperbolicCompositeCurve3();
    HyperbolicCompositeCurve3& operator =(const HyperbolicCompositeCurve3 &rhs);

    GLboolean continueExistingArc(GLuint index, Direction direction);
    GLboolean joinExistingArcs(GLuint firstIndex, GLuint secondIndex, Direction firstDirection, Direction secondDirection);
    GLboolean mergeExistingArcs(GLuint firstIndex, GLuint secondIndex, Direction firstDirection, Direction secondDirection);
    GLboolean eraseExistingArc(GLuint index);
    void renderImage();
    void renderControlPoint();
    void renderControlPointColored(GLuint index);
    void renderFirstOrderDerivatives();
    void renderSecondOrderDerivatives();
    GLboolean insertDefaultArc();
    void shiftExistingArc(GLuint index, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ);
    void shiftExistingArc(ArcAttributes &attr, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ, const ArcAttributes *initial);
    ArcAttributes& getAtIndex(GLuint index);
    void modifyExistingArc(GLuint index, GLuint pointIndex, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ);
    void setColor(GLuint index, Color4 *color);
    void setAlpha(GLdouble alpha);
    GLdouble getAlpha();
    GLboolean print(std::string filname);
    GLuint getAttributesSize();
    GLboolean read(std::string filname);
    GLuint findIndex(ArcAttributes &attr);

private:
    void updateAttributesWhenArcChanged(GLuint index);
    void updateAttributesWhenArcChanged(ArcAttributes &attr);
    void updateNeighors(ArcAttributes &attr, Direction direction, GLuint index);

};


}
