#include "hyperbolicarcs3.h"

using namespace cagd;

HyperbolicArc3::HyperbolicArc3(GLdouble alpha, GLenum data_usage_flag):
    LinearCombination3(0.0, alpha, 4, data_usage_flag)
{
    _alpha = alpha;
    setConstants();
}

GLboolean HyperbolicArc3::setAlpha(GLdouble alpha)
{
    if (alpha <= 0)
        return GL_FALSE;

    SetDefinitionDomain(0, alpha);
    _alpha = alpha;
    setConstants();
    return GL_TRUE;
}

GLdouble HyperbolicArc3::getAlpha()
{
    return _alpha;
}

void HyperbolicArc3::setConstants()
{
    GLdouble alpha_2 = _alpha / 2, denominator = pow(sinh(alpha_2), 4);
    _alpha_constant1 = 4.0 * cosh(alpha_2) / denominator;
    _alpha_constant2 = (1.0 + 2.0 * pow(cosh(alpha_2), 2)) / denominator;
    _alpha_constant3 = 1.0 / denominator;
}

GLboolean HyperbolicArc3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const
{
    values.ResizeColumns(4);

    if (u < 0 || u > _alpha)
        return GL_FALSE;

    values(0) = blendingFunction0(u);
    values(0) = blendingFunction1(u);
    values(0) = blendingFunction2(u);
    values(0) = blendingFunction3(u);

    return GL_TRUE;
}

GLboolean HyperbolicArc3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const
{
    d.ResizeRows(max_order_of_derivatives + 1);
    d.LoadNullVectors();

    Matrix<GLdouble> dF(max_order_of_derivatives + 1, _data.GetRowCount());

    //0.th order derivatives in 1,2,3,4. control points
    if (max_order_of_derivatives >= 0)
    {
        dF(0, 0) = blendingFunction0(u);
        dF(0, 1) = blendingFunction1(u);
        dF(0, 2) = blendingFunction2(u);
        dF(0, 3) = blendingFunction3(u);
    }
    if (max_order_of_derivatives >= 1)
    {
        dF(1, 0) = blendingFunction0_1(u);
        dF(1, 1) = blendingFunction1_1(u);
        dF(1, 2) = blendingFunction2_1(u);
        dF(1, 3) = blendingFunction3_1(u);
    }
    if(max_order_of_derivatives >= 2)
    {
        dF(2, 0) = blendingFunction0_2(u);
        dF(2, 1) = blendingFunction1_2(u);
        dF(2, 2) = blendingFunction2_2(u);
        dF(2, 3) = blendingFunction3_2(u);
    }

    for (GLuint r = 0; r <= max_order_of_derivatives; ++r) {

        for (GLuint i = 0; i < _data.GetRowCount(); ++i) {
               d[r] += _data[i] * dF(r, i);
        }
    }

    return GL_TRUE;
}

GLdouble HyperbolicArc3::blendingFunction0(GLdouble u) const
{
    return blendingFunction3(_alpha - u);
}

GLdouble HyperbolicArc3::blendingFunction1(GLdouble u) const
{
    return blendingFunction2(_alpha - u);
}

GLdouble HyperbolicArc3::blendingFunction2(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2, u_2 = u / 2;
    return _alpha_constant1 * sinh(alpha_u_2) * pow(sinh(u_2), 3) +
            _alpha_constant2 * pow(sinh(alpha_u_2), 2) * pow(sinh(u_2), 2);
}

GLdouble HyperbolicArc3::blendingFunction3(GLdouble u) const
{
    return _alpha_constant3 * pow(sinh(u / 2), 4);
}

//first order derivatives
GLdouble HyperbolicArc3::blendingFunction0_1(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2;
    return -2 * _alpha_constant3 * pow(sinh(alpha_u_2), 3) * cosh(alpha_u_2);;
}

GLdouble HyperbolicArc3::blendingFunction1_1(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2, u_2 = u / 2;
    return 1.0/2 * _alpha_constant1 * cosh(u_2) * pow(sinh(alpha_u_2), 3) - 3.0/2 * _alpha_constant1 * sinh(u_2)
            * pow(sinh(alpha_u_2), 2) * cosh(alpha_u_2) + _alpha_constant2 * sinh(u_2) * cosh(u_2) * pow(sinh(alpha_u_2), 2)
            - _alpha_constant2 * pow(sinh(u_2), 2)* sinh(alpha_u_2) * cosh(alpha_u_2);
}

GLdouble HyperbolicArc3::blendingFunction2_1(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2, u_2 = u / 2;
    return -1.0/2 * _alpha_constant1 * pow(sinh(u_2), 3) * cosh(alpha_u_2) + 3.0/2 * _alpha_constant1 * pow(sinh(u_2), 2)
            * cosh(u_2) * sinh(alpha_u_2) - _alpha_constant2 * pow(sinh(u_2), 2) * sinh(alpha_u_2) * cosh(alpha_u_2)
            + _alpha_constant2 * sinh(u_2) * cosh(u_2) * pow(sinh(alpha_u_2), 2);;
}

GLdouble HyperbolicArc3::blendingFunction3_1(GLdouble u) const
{
    return 2 * _alpha_constant3 * pow(sinh(u / 2), 3) * cosh(u / 2);
}

//second order derivatives
GLdouble HyperbolicArc3::blendingFunction0_2(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2;
    //return -2 * _alpha_constant3 * pow(sinh(alpha_u_2), 3) * cosh(alpha_u_2);
        return _alpha_constant3 * pow(sinh(alpha_u_2),2) * (pow(sinh(alpha_u_2), 2) + 3 * pow(cosh(alpha_u_2),2));
}

GLdouble HyperbolicArc3::blendingFunction1_2(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2, u_2 = u / 2;

        //a sinh^2((b - u)/2) (0.25 sinh(u/2) sinh((b - u)/2) - 0.75 cosh(u/2) cosh((b - u)/2))
        //a sinh((b - u)/2) (0.75 sinh(u/2) sinh^2((b - u)/2) + 1.5 sinh(u/2) cosh^2((b - u)/2) - 0.75 cosh(u/2) sinh((b - u)/2) cosh((b - u)/2))

        return    _alpha_constant1 * pow(sinh(alpha_u_2),2) * (0.25 * sinh(u_2) * sinh(alpha_u_2) - 0.75 * cosh(u_2) * cosh(alpha_u_2))
                + _alpha_constant1 * sinh(alpha_u_2) * (0.75 * sinh(u_2) * pow(sinh(alpha_u_2),2) + 1.5 * sinh(u_2) * pow(cosh(alpha_u_2),2) - 0.75 * cosh(u_2) * sinh(alpha_u_2) * cosh(alpha_u_2))
                + 1.0/2 * _alpha_constant2 * sinh(alpha_u_2) * pow(sinh(u_2),2) * sinh(alpha_u_2) + pow(cosh(u_2), 2) * sinh(alpha_u_2) - 2 * sinh(u_2) * cosh(u_2) * cosh(alpha_u_2)
                + 1.0/2 * _alpha_constant2 * sinh(u_2) * (sinh(u_2) * pow(sinh(alpha_u_2),2) + sinh(u_2) * pow(cosh(alpha_u_2),2) - 2 * cosh(u_2) * sinh(alpha_u_2) * cosh(alpha_u_2));
}

GLdouble HyperbolicArc3::blendingFunction2_2(GLdouble u) const
{
    GLdouble alpha_u_2 = (_alpha - u) / 2, u_2 = u / 2;

        //a sinh^2(u/2) (0.25 sinh(u/2) sinh((b - u)/2) - 0.75 cosh(u/2) cosh((b - u)/2))
        //a sinh(u/2) (0.75 sinh^2(u/2) sinh((b - u)/2) + 1.5 cosh^2(u/2) sinh((b - u)/2) - 0.75 sinh(u/2) cosh(u/2) cosh((b - u)/2))
        //1/2 a sinh(u/2) (sinh(u/2) sinh^2((b - u)/2) + sinh(u/2) cosh^2((b - u)/2) - 2 cosh(u/2) sinh((b - u)/2) cosh((b - u)/2))

        return    _alpha_constant1 * pow(sinh(u_2),2) * (0.25 * sinh(u_2) * sinh(alpha_u_2) - 0.75 * cosh(u_2) * cosh(alpha_u_2))
                        + _alpha_constant1 * sinh(u_2) * (0.75 * pow(sinh(u_2),2) * sinh(alpha_u_2) + 1.5 * pow(cosh(u_2),2) * sinh(alpha_u_2) - 0.75 * sinh(u_2) * cosh(u_2) * cosh(alpha_u_2))
                        + 1.0/2 * _alpha_constant2 * sinh(u_2) * (sinh(u_2) * pow(sinh(alpha_u_2),2) + sinh(u_2) * pow(cosh(alpha_u_2),2) - 2 * cosh(u_2) * sinh(alpha_u_2) * cosh(alpha_u_2))
                        + 1.0/2 * _alpha_constant2 * sinh(alpha_u_2) * (pow(sinh(u_2),2) * sinh(alpha_u_2) + pow(cosh(u_2),2) * sinh(alpha_u_2) - 2 * sinh(u_2) * cosh(u_2) * cosh(alpha_u_2));
}

GLdouble HyperbolicArc3::blendingFunction3_2(GLdouble u) const
{
        GLdouble u_2 = u / 2;
        //a sinh^2(u/2) (sinh^2(u/2) + 3 cosh^2(u/2))

        return _alpha_constant3 * pow(sinh(u_2),2) * (pow(sinh(u_2),2) + 3 * pow(cosh(u_2),2));
}
