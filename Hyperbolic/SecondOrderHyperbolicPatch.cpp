#include "SecondOrderHyperbolicPatch.h"
#include <math.h>
#include <iostream>

using namespace std;
using namespace cagd;

SecondOrderHyperbolic::SecondOrderHyperbolic(GLdouble alpha):TensorProductSurface3(0.0, alpha, 0.0, alpha, 4, 4), _alpha(alpha)
{
}

GLboolean SecondOrderHyperbolic::UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble> &blending_values) const
{
    if(u_knot < 0.0 || u_knot > _alpha)
    {
        return GL_FALSE;
    }

    blending_values.ResizeColumns(4);

    GLdouble u = u_knot, alpha_2 = _alpha / 2, alpha_u_2 = (_alpha - u) / 2, u_2 = u / 2;
    GLdouble const1 = 4.0 * cosh(alpha_2) / pow(sinh(alpha_2), 4);
    GLdouble const2 = (1.0 + 2.0 * pow(cosh(alpha_2), 2)) / pow(sinh(alpha_2), 4);
    GLdouble const3 = 1.0 / pow(sinh(alpha_2), 4);

    blending_values(0) = const3 * pow(sinh(alpha_u_2), 4);
    blending_values(1) = const1 * sinh(u_2) * pow(sinh(alpha_u_2), 3) + const2 * pow(sinh(u_2), 2) * pow(sinh(alpha_u_2), 2);
    blending_values(2) = const1 * sinh(alpha_u_2) * pow(sinh(u_2), 3) + const2 * pow(sinh(alpha_u_2), 2) * pow(sinh(u_2), 2);
    blending_values(3) = const3 * pow(sinh(u_2), 4);

    return GL_TRUE;
}

GLboolean SecondOrderHyperbolic::VBlendingFunctionValues(GLdouble v_knot, RowMatrix<GLdouble>& blending_values) const
{
    if(v_knot < 0.0 || v_knot > _alpha)
    {
        return GL_FALSE;
    }

    blending_values.ResizeColumns(4);

    GLdouble v = v_knot, alpha_2 = _alpha / 2.0, alpha_v_2 = (_alpha - v) / 2, v_2 = v / 2;
    GLdouble const1 = 4.0 * cosh(alpha_2) / pow(sinh(alpha_2), 4);
    GLdouble const2 = (1 + 2 * pow(cosh(alpha_2), 2)) / pow(sinh(alpha_2), 4);
    GLdouble const3 = 1.0 / pow(sinh(alpha_2), 4);

    blending_values(0) = const3 * pow(sinh(alpha_v_2), 4);
    blending_values(1) = const1 * sinh(v_2) * pow(sinh(alpha_v_2), 3) + const2 * pow(sinh(v_2), 2) * pow(sinh(alpha_v_2), 2);
    blending_values(2) = const1 * sinh(alpha_v_2) * pow(sinh(v_2), 3) + const2 * pow(sinh(alpha_v_2), 2) * pow(sinh(v_2), 2);
    blending_values(3) = const3 * pow(sinh(v_2), 4);

    return GL_TRUE;
}

GLboolean SecondOrderHyperbolic::CalculatePartialDerivatives(GLuint maximum_order_of_partial_derivatives, GLdouble u, GLdouble v, PartialDerivatives &pd) const
{
    if(u < 0.0 || u > _alpha || v < 0.0 || v > _alpha || maximum_order_of_partial_derivatives > 1)
    {
        return GL_FALSE;
    }

    RowMatrix<GLdouble> u_blending_values(4), d1_u_blending_values(4);

    GLdouble alpha_2 = _alpha / 2.0, alpha_u_2 = (_alpha - u) / 2.0, u_2 = u / 2.0;
    GLdouble const1 = 4.0 * cosh(alpha_2) / pow(sinh(alpha_2), 4);
    GLdouble const2 = (1.0 + 2.0 * pow(cosh(alpha_2), 2)) / pow(sinh(alpha_2), 4);
    GLdouble const3 = 1.0 / pow(sinh(alpha_2), 4);

    u_blending_values(0) = const3 * pow(sinh(alpha_u_2), 4);
    u_blending_values(1) = const1 * sinh(u_2) * pow(sinh(alpha_u_2), 3) + const2 * pow(sinh(u_2), 2) * pow(sinh(alpha_u_2), 2);
    u_blending_values(2) = const1 * sinh(alpha_u_2) * pow(sinh(u_2), 3) + const2 * pow(sinh(alpha_u_2), 2) * pow(sinh(u_2), 2);
    u_blending_values(3) = const3 * pow(sinh(u_2), 4);

    d1_u_blending_values(0) = -2 * const3 * pow(sinh(alpha_u_2), 3) * cosh(alpha_u_2);

    d1_u_blending_values(1) = 1.0/2 * const1 * cosh(u_2) * pow(sinh(alpha_u_2), 3)
            - 3.0/2 * const1 * sinh(u_2) * pow(sinh(alpha_u_2), 2) * cosh(alpha_u_2)
            + const2 * sinh(u_2) * cosh(u_2) * pow(sinh(alpha_u_2), 2)
            - const2 * pow(sinh(u_2), 2)* sinh(alpha_u_2) * cosh(alpha_u_2);

    d1_u_blending_values(2) =  -1.0/2.0 * const1 * pow(sinh(u_2), 3) * cosh(alpha_u_2)
            + 3.0/2 * const1 * pow(sinh(u_2), 2) * cosh(u_2) * sinh(alpha_u_2)
            - const2 * pow(sinh(u_2), 2) * sinh(alpha_u_2) * cosh(alpha_u_2)
            + const2 * sinh(u_2) * cosh(u_2) * pow(sinh(alpha_u_2), 2);


    d1_u_blending_values(3) = 2.0 * const3 * pow(sinh(u_2), 3) * cosh(u_2);

    RowMatrix<GLdouble> v_blending_values(4), d1_v_blending_values(4);

    //========= v =========
    GLdouble alpha_v_2 = (_alpha - v) / 2, v_2 = v / 2;

    v_blending_values(0) = const3 * pow(sinh(alpha_v_2), 4);
    v_blending_values(1) = const1 * sinh(v_2) * pow(sinh(alpha_v_2), 3) + const2 * pow(sinh(v_2), 2) * pow(sinh(alpha_v_2), 2);
    v_blending_values(2) = const1 * sinh(alpha_v_2) * pow(sinh(v_2), 3) + const2 * pow(sinh(alpha_v_2), 2) * pow(sinh(v_2), 2);
    v_blending_values(3) = const3 * pow(sinh(v_2), 4);

    d1_v_blending_values(0) = -2 * const3 * pow(sinh(alpha_v_2), 3) * cosh(alpha_v_2);
    d1_v_blending_values(1) = 1.0/2 * const1 * cosh(v_2) * pow(sinh(alpha_v_2), 3) - 3.0/2 * const1 * sinh(v_2)
            * pow(sinh(alpha_v_2), 2) * cosh(alpha_v_2) + const2 * sinh(v_2) * cosh(v_2) * pow(sinh(alpha_v_2), 2)
            - const2 * pow(sinh(v_2), 2)* sinh(alpha_v_2) * cosh(alpha_v_2);
    d1_v_blending_values(2) =  -1.0/2 * const1 * pow(sinh(v_2), 3) * cosh(alpha_v_2) + 3.0/2 * const1 * pow(sinh(v_2), 2)
            * cosh(v_2) * sinh(alpha_v_2) - const2 * pow(sinh(v_2), 2) * sinh(alpha_v_2) * cosh(alpha_v_2)
            + const2 * sinh(v_2) * cosh(v_2) * pow(sinh(alpha_v_2), 2);
    d1_v_blending_values(3) = 2 * const3 * pow(sinh(v_2), 3) * cosh(v_2);

    pd.ResizeRows(2);
    pd.LoadNullVectors();

    for(GLuint row = 0; row < 4; ++row)
    {
        DCoordinate3 aux_d0_v, aux_d1_v;
        for(GLuint column = 0; column < 4; ++column)
        {
            aux_d0_v += _data(row, column) * v_blending_values(column);
            aux_d1_v += _data(row, column) * d1_v_blending_values(column);
        }
        pd(0, 0) += aux_d0_v * u_blending_values(row);
        pd(1, 0) += aux_d0_v * d1_u_blending_values(row);
        pd(1, 1) += aux_d1_v * u_blending_values(row);
    }

//    cout << "u = " << u << "v = " << v << " point:  " << pd(0, 0) << endl;

    return GL_TRUE;
}

void SecondOrderHyperbolic::set_alpha(double alpha)
{
    _alpha = alpha;
    SetUInterval(0, _alpha);
    SetVInterval(0, _alpha);
}

GLdouble SecondOrderHyperbolic::get_alpha()
{
    return _alpha;
}
