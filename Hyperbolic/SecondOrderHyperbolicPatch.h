#pragma once

#include "../Core/TensorProductSurfaces3.h"

namespace cagd
{
    class SecondOrderHyperbolic: public TensorProductSurface3
    {
    protected:
        GLdouble _alpha;
    public:
        SecondOrderHyperbolic(GLdouble alpha = 1.0);
        GLboolean UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble>& blending_values) const;
        GLboolean VBlendingFunctionValues(GLdouble v_knot, RowMatrix<GLdouble>& blending_values) const;
        GLboolean CalculatePartialDerivatives(GLuint maximum_order_of_partial_derivatives, GLdouble u, GLdouble v, PartialDerivatives& pd) const;

        void set_alpha(double alpha);
        GLdouble get_alpha();
    };
}
