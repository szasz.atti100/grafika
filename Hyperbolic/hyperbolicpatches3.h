#pragma once
#include "../Core/TensorProductSurfaces3.h"

namespace cagd
{
    class HyperbolicPatch3:public TensorProductSurface3
    {
        protected:
            GLdouble _alpha_u, _alpha_v; //possible shape parameters
        public:
            SomePatch3(GLdouble alpha_u, GLdouble alpha_v);
            GLboolean UBlendingFunctionValues(GLdouble u, RowMatrix<GLdouble>& values) const; //ide jon a gorbe blendingje
            GLboolean VBlendingFunctionValues(GLdouble v, RowMatrix<GLdouble>& values) const; //ide jon a gorbe blendingje

            GLboolean CalculatePartialDerivatives(GLuint max_order_derivatives,
                GLdouble u, GLdouble v,
                PartialDerivatives &pd) const; //kurzusban bzre van pelda

            //setters/getters
            //nalam az alohakat kell karbantartani
            //set0, setv, setu, setuv
    };
}
