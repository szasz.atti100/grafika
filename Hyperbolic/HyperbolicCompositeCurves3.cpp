#include "hyperboliccompositecurves3.h"
#include "../Core/DCoordinates3.h"
#include <iostream>
#include <fstream>

using namespace std;

namespace cagd{

HyperbolicCompositeCurve3::ArcAttributes::ArcAttributes()
{
    _arc      = nullptr;
    _arc_image    = nullptr;
    _color    = &Color4Red;
    _next     = nullptr;
    _previous = nullptr;
}

HyperbolicCompositeCurve3::ArcAttributes::ArcAttributes(const ArcAttributes &rhs) :
    _arc(rhs._arc ? new HyperbolicArc3(*rhs._arc) : nullptr),
    _arc_image(rhs._arc_image ? new GenericCurve3(*rhs._arc_image) : nullptr),
    _color(rhs._color),
    _next(rhs._next),
    _previous(rhs._previous)
{}

HyperbolicCompositeCurve3::ArcAttributes& HyperbolicCompositeCurve3::ArcAttributes::operator =(const ArcAttributes& rhs)
{
    if (this != &rhs) {
        if (_arc)
            delete _arc, _arc = nullptr;

        if (_arc_image)
            delete _arc_image, _arc_image = nullptr;

        _arc  = rhs._arc ? new HyperbolicArc3(*rhs._arc) : nullptr;
        _arc_image= rhs._arc_image ? new GenericCurve3(*rhs._arc_image) : nullptr;
        _color = rhs._color;
        _next = rhs._next;
        _previous = rhs._previous;
    }
    return *this;
}

HyperbolicCompositeCurve3::ArcAttributes::~ArcAttributes()
{
    if(_arc)
        delete _arc, _arc = nullptr;
    if(_arc_image)
        delete _arc_image, _arc_image = nullptr;
}

HyperbolicCompositeCurve3::HyperbolicCompositeCurve3(GLdouble alpha, GLuint maxArcCapacity)
{
    _alpha = alpha;
    _maxArcCapacity = maxArcCapacity;
    _attributes.reserve(maxArcCapacity);
}

HyperbolicCompositeCurve3::HyperbolicCompositeCurve3(const HyperbolicCompositeCurve3 &rhs) :
    _alpha(rhs._alpha),
    _maxArcCapacity(rhs._maxArcCapacity),
    _attributes(rhs._attributes)
{}

HyperbolicCompositeCurve3::~HyperbolicCompositeCurve3()
{
    _attributes.clear();
}


HyperbolicCompositeCurve3& HyperbolicCompositeCurve3::operator =(const HyperbolicCompositeCurve3 &rhs)
{
    if (this != &rhs)
    {
        _alpha = rhs._alpha;
        _maxArcCapacity = rhs._maxArcCapacity;
        _attributes = rhs._attributes;
    }
    return *this;
}

GLboolean HyperbolicCompositeCurve3::continueExistingArc(GLuint index, Direction direction)
{
    if (index < 0 || index >= _attributes.size())
    {
        return GL_FALSE;
    }

    GLuint n = _attributes.size();
    _attributes.resize(n + 1);
    _attributes[n]._arc = new HyperbolicArc3(_alpha);

    if(!_attributes[n]._arc)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    if(direction == RIGHT)
    {
        if(_attributes[index]._next)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }

        (*_attributes[n]._arc)[0] = (*_attributes[index]._arc)[3];
        (*_attributes[n]._arc)[1] = 2.0 * (*_attributes[index]._arc)[3] - (*_attributes[index]._arc)[2];
        (*_attributes[n]._arc)[2] = 3.0 * (*_attributes[index]._arc)[3] - (*_attributes[index]._arc)[2];
        (*_attributes[n]._arc)[3] = 4.0 * (*_attributes[index]._arc)[3] - (*_attributes[index]._arc)[2];

        _attributes[n]._previous = &_attributes[index];
        _attributes[index]._next = &_attributes[n];
    }

    if(direction == LEFT)
    {
        if(_attributes[index]._previous)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }

        (*_attributes[n]._arc)[0] = (*_attributes[index]._arc)[0];
        (*_attributes[n]._arc)[1] = 2.0 * (*_attributes[index]._arc)[0] - (*_attributes[index]._arc)[1];
        (*_attributes[n]._arc)[2] = 3.0 * (*_attributes[index]._arc)[0] - (*_attributes[index]._arc)[1];
        (*_attributes[n]._arc)[3] = 4.0 * (*_attributes[index]._arc)[0] - (*_attributes[index]._arc)[1];

        _attributes[n]._next = &_attributes[index];
        _attributes[index]._previous = &_attributes[n];
    }

    _attributes[n]._arc->UpdateVertexBufferObjectsOfData();
    updateAttributesWhenArcChanged(n);

    _attributes[n]._color = new Color4(1.0f, 1.0f, 1.0f, 0.0f);

    _attributes[n]._previous = nullptr;


    return GL_TRUE;
}

GLboolean HyperbolicCompositeCurve3::joinExistingArcs(GLuint firstIndex, GLuint secondIndex, Direction firstDirection, Direction secondDirection)
{
    std::cout << firstIndex << " " << secondIndex << " " << firstDirection << " " << secondDirection << std::endl;
    //std::vector<ArcAttributes> *initial_address = &_attributes;
    if(firstIndex == secondIndex || firstIndex < 0 || secondIndex < 0 || firstIndex > _attributes.size() || secondIndex > _attributes.size())
    {
        return GL_FALSE;
    }

    GLuint n = (GLuint)_attributes.size();

    _attributes.resize(n + 1);
    _attributes[n]._arc = new HyperbolicArc3(_alpha);

    if (!_attributes[n]._arc)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }


    if(firstDirection == RIGHT)
    {
        if(_attributes[firstIndex]._next)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }

        (*_attributes[n]._arc)[0] = (*_attributes[firstIndex]._arc)[3];
        (*_attributes[n]._arc)[1] = 2.0 * (*_attributes[firstIndex]._arc)[3] - (*_attributes[firstIndex]._arc)[2];

        _attributes[n]._previous = &_attributes[firstIndex];
        _attributes[firstIndex]._next = &_attributes[n];
    }

    if(firstDirection == LEFT)
    {
        if(_attributes[firstIndex]._previous)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }

        (*_attributes[n]._arc)[0] = (*_attributes[firstIndex]._arc)[0];
        (*_attributes[n]._arc)[1] = 2.0 * (*_attributes[firstIndex]._arc)[0] - (*_attributes[firstIndex]._arc)[1];

        _attributes[n]._next = &_attributes[firstIndex];
        _attributes[firstIndex]._previous = &_attributes[n];
    }

    if(secondDirection == LEFT)
    {
        if(_attributes[secondIndex]._previous)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }
        (*_attributes[n]._arc)[3] = (*_attributes[secondIndex]._arc)[0];
        (*_attributes[n]._arc)[2] = 2.0 * (*_attributes[secondIndex]._arc)[0] - (*_attributes[secondIndex]._arc)[1];

        _attributes[n]._next = &_attributes[secondIndex];
        _attributes[secondIndex]._previous = &_attributes[n];
    }

    if(secondDirection == RIGHT)
    {
        if(_attributes[secondIndex]._next)
        {
            _attributes.pop_back();
            return GL_FALSE;
        }
        (*_attributes[n]._arc)[3] = (*_attributes[secondIndex]._arc)[3];
        (*_attributes[n]._arc)[2] = 2.0 * (*_attributes[secondIndex]._arc)[3] - (*_attributes[secondIndex]._arc)[2];

        _attributes[n]._previous = &_attributes[secondIndex];
        _attributes[secondIndex]._next = &_attributes[n];
    }

    _attributes[n]._arc->UpdateVertexBufferObjectsOfData();

    updateAttributesWhenArcChanged(n);

    _attributes[n]._color = new Color4(1.0f, 1.0f, 1.0f, 0.0f);
    return GL_TRUE;
}

GLboolean HyperbolicCompositeCurve3::mergeExistingArcs(GLuint firstIndex, GLuint secondIndex, Direction firstDirection, Direction secondDirection)
{
    DCoordinate3 point1, point2;
    if(firstDirection == RIGHT)
    {
        if(_attributes[firstIndex]._next)
            return GL_FALSE;
    }
    if(firstDirection == LEFT)
    {
        if(_attributes[firstIndex]._previous)
            return GL_FALSE;
    }
    if(secondDirection == RIGHT)
    {
        if(_attributes[secondIndex]._next)
            return GL_FALSE;
    }
    if(secondDirection == LEFT)
    {
        if(_attributes[secondIndex]._previous)
            return GL_FALSE;
    }


    if(firstDirection == RIGHT && secondDirection == LEFT)
    {
        point1 = (*_attributes[firstIndex]._arc)[2];
        point2 = (*_attributes[secondIndex]._arc)[1];

        (*_attributes[firstIndex]._arc)[3] = (point1 + point2) / 2.0;
        (*_attributes[secondIndex]._arc)[0] = (point1 + point2) / 2.0;

        _attributes[firstIndex]._next = &_attributes[secondIndex];
        _attributes[secondIndex]._previous = &_attributes[firstIndex];
    }

    if(firstDirection == RIGHT && secondDirection == RIGHT)
    {
        point1 = (*_attributes[firstIndex]._arc)[2];
        point2 = (*_attributes[secondIndex]._arc)[2];

        (*_attributes[firstIndex]._arc)[3] = (point1 + point2) / 2.0;
        (*_attributes[secondIndex]._arc)[3] = (point1 + point2) / 2.0;

        _attributes[firstIndex]._next = &_attributes[secondIndex];
        _attributes[secondIndex]._next = &_attributes[firstIndex];
    }

    if(firstDirection == LEFT && secondDirection == LEFT)
    {
        point1 = (*_attributes[firstIndex]._arc)[1];
        point2 = (*_attributes[secondIndex]._arc)[1];

        (*_attributes[firstIndex]._arc)[0] = (point1 + point2) / 2.0;
        (*_attributes[secondIndex]._arc)[0] = (point1 + point2) / 2.0;

        _attributes[firstIndex]._previous = &_attributes[secondIndex];
        _attributes[secondIndex]._previous = &_attributes[firstIndex];
    }

    if(firstDirection == LEFT && secondDirection == RIGHT)
    {
        point1 = (*_attributes[firstIndex]._arc)[1];
        point2 = (*_attributes[secondIndex]._arc)[2];

        (*_attributes[firstIndex]._arc)[0] = (point1 + point2) / 2.0;
        (*_attributes[secondIndex]._arc)[3] = (point1 + point2) / 2.0;

        _attributes[firstIndex]._previous = &_attributes[secondIndex];
        _attributes[secondIndex]._next = &_attributes[firstIndex];
    }

    updateAttributesWhenArcChanged(firstIndex);
    updateAttributesWhenArcChanged(secondIndex);

    return GL_TRUE;
}

GLboolean HyperbolicCompositeCurve3::eraseExistingArc(GLuint index)
{
    if(index < 0 || index >= _attributes.size())
        return GL_FALSE;

    GLuint n = _attributes.size() - 1;

    if(_attributes[index]._next)
    {
        if(_attributes[index]._next->_next == &_attributes[index])
        {
            _attributes[index]._next->_next = nullptr;
        } else
        {
            _attributes[index]._next->_previous = nullptr;
        }
    }

    if(_attributes[index]._previous)
    {
        if(_attributes[index]._previous->_next == &_attributes[index])
        {
            _attributes[index]._previous->_next = nullptr;
        } else
        {
            _attributes[index]._previous->_previous = nullptr;
        }
    }

    if(_attributes[index]._arc)
    {
        delete _attributes[index]._arc, _attributes[index]._arc = nullptr;
    }

    if(_attributes[index]._arc_image)
    {
        delete _attributes[index]._arc_image, _attributes[index]._arc_image = nullptr;
    }

    ArcAttributes *end_address = &_attributes[n];
    ArcAttributes *index_address = &_attributes[index];

    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        if(_attributes[i]._previous == end_address)
            _attributes[i]._previous = index_address;
        if(_attributes[i]._next == end_address)
            _attributes[i]._next = index_address;
    }

    ArcAttributes temp(_attributes[n]);
    _attributes[n] = _attributes[index];
    _attributes[index] = temp;
    _attributes.pop_back();


    _attributes[index]._arc->UpdateVertexBufferObjectsOfData();
    updateAttributesWhenArcChanged(_attributes[index]);

    return GL_TRUE;
}

void HyperbolicCompositeCurve3::renderImage()
{
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        if(_attributes[i]._arc_image)
        {
            if(_attributes[i]._color){
                glColor3f(_attributes[i]._color->r(), _attributes[i]._color->g(), _attributes[i]._color->b());
                std::cout << _attributes[i]._color->r() << _attributes[i]._color->g() << _attributes[i]._color->b() << std::endl;
            }
            _attributes[i]._arc_image->RenderDerivatives(0, GL_LINE_STRIP);
            glPointSize(10.0f);
            _attributes[i]._arc->RenderDataColored(GL_POINTS, 2);
            glPointSize(0.0f);

        }
    }
}

void HyperbolicCompositeCurve3::renderControlPoint()
{
    glColor3f(1.0f, 0.5f, 0.3f);
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._arc->RenderData();
    }
}

void HyperbolicCompositeCurve3::renderControlPointColored(GLuint index)
{
    glColor3f(1.0f, 0.5f, 0.3f);
    glPointSize(10.0f);
    _attributes[index]._arc->RenderData();
    _attributes[index]._arc->RenderDataColored(GL_POINTS, 4);
    glPointSize(10.0f);
}

void HyperbolicCompositeCurve3::renderFirstOrderDerivatives()
{
    glColor3f(0.0f, 1.0f, 1.0f);
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._arc_image->RenderDerivatives(1, GL_LINES);
    }
}

GLdouble HyperbolicCompositeCurve3::getAlpha()
{
    return _alpha;
}

void HyperbolicCompositeCurve3::renderSecondOrderDerivatives()
{
    glColor3f(1.0f, 0.0f, 1.0f);
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._arc_image->RenderDerivatives(2, GL_LINES);
    }
}

void HyperbolicCompositeCurve3::setColor(GLuint index, Color4 *color)
{
    _attributes[index]._color = color;
}

GLboolean HyperbolicCompositeCurve3::insertDefaultArc()
{
    cout << "In insertDefaultArc()" << endl;
    std::vector<ArcAttributes> *initial_address = &_attributes;

    GLuint n = (GLuint)_attributes.size();

    _attributes.resize(n + 1);
    _attributes[n]._arc = new HyperbolicArc3(_alpha);

    std::cout<< _attributes.size() << std::endl;
    if (!_attributes[n]._arc)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }

    (*_attributes[n]._arc)[0] = DCoordinate3(0, 0, 0);
    (*_attributes[n]._arc)[1] = DCoordinate3(0, 2, 2);
    (*_attributes[n]._arc)[2] = DCoordinate3(-2, 2, 0);
    (*_attributes[n]._arc)[3] = DCoordinate3(2, 5, 2);

    _attributes[n]._arc->UpdateVertexBufferObjectsOfData();

    _attributes[n]._arc_image = _attributes[n]._arc->GenerateImage(2, 30);

    if(!_attributes[n]._arc_image)
    {
        _attributes.pop_back();
        return GL_FALSE;
    }
    _attributes[n]._arc_image->UpdateVertexBufferObjects();
    _attributes[n]._color = new Color4(1.0f, 1.0f, 1.0f, 0.0f);

    _attributes[n]._next = nullptr;
    _attributes[n]._previous = nullptr;

    return GL_TRUE;
}

HyperbolicCompositeCurve3::ArcAttributes& HyperbolicCompositeCurve3::getAtIndex(GLuint index)
{
    return _attributes[index];
}


void HyperbolicCompositeCurve3::shiftExistingArc(ArcAttributes &attr, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ, const ArcAttributes *initial)
{
    GLdouble x, y, z;
    for(GLuint i = 0; i < 4; i++)
    {
        x = (*attr._arc)[i].x();
        y = (*attr._arc)[i].y();
        z = (*attr._arc)[i].z();

        x += shiftX;
        y += shiftY;
        z += shiftZ;

        (*attr._arc)[i] = DCoordinate3(x, y, z);
    }

    attr._arc->UpdateVertexBufferObjectsOfData();

    //TO DO call update function instead
    if(attr._arc_image)
    {
        delete attr._arc_image, attr._arc_image = nullptr;
    }

    attr._arc_image = attr._arc->GenerateImage(2, 30);
    if(!attr._arc_image)
    {
        //TO DO exception
    }
    attr._arc_image->UpdateVertexBufferObjects();

    if(attr._next && attr._next != initial)
    {
        shiftExistingArc(*attr._next, shiftX, shiftY, shiftZ, &attr);
    }
    if(attr._previous && attr._previous != initial)
    {
        shiftExistingArc(*attr._previous, shiftX, shiftY, shiftZ, &attr);
    }

}

void HyperbolicCompositeCurve3::shiftExistingArc(GLuint index, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ)
{
    //std::cout<<"helloka" << std::endl;
    GLdouble x, y, z;
    for(GLuint i = 0; i < 4; i++)
    {
        x = (*_attributes[index]._arc)[i].x();
        y = (*_attributes[index]._arc)[i].y();
        z = (*_attributes[index]._arc)[i].z();

        x += shiftX;
        y += shiftY;
        z += shiftZ;

        (*_attributes[index]._arc)[i] = DCoordinate3(x, y, z);
    }

    _attributes[index]._arc->UpdateVertexBufferObjectsOfData();
    updateAttributesWhenArcChanged(index);
     if(_attributes[index]._next)
    {
        shiftExistingArc(*_attributes[index]._next, shiftX, shiftY, shiftZ, &_attributes[index]);
    }
    if(_attributes[index]._previous)
    {
        shiftExistingArc(*_attributes[index]._previous, shiftX, shiftY, shiftZ, &_attributes[index]);
    }

}

void HyperbolicCompositeCurve3::updateAttributesWhenArcChanged(GLuint index)
{
    _attributes[index]._arc->UpdateVertexBufferObjectsOfData();
    if(_attributes[index]._arc_image)
    {
        delete _attributes[index]._arc_image, _attributes[index]._arc_image = nullptr;
    }

    _attributes[index]._arc_image = _attributes[index]._arc->GenerateImage(2, 30);
    if(!_attributes[index]._arc_image)
    {
        //TO DO exception
    }
    _attributes[index]._arc_image->UpdateVertexBufferObjects();
}

void HyperbolicCompositeCurve3::updateAttributesWhenArcChanged(ArcAttributes &attr)
{
    if(attr._arc_image)
    {
        delete attr._arc_image, attr._arc_image = nullptr;
    }

    attr._arc_image = attr._arc->GenerateImage(2, 30);
    if(!attr._arc_image)
    {
        //TO DO exception
    }
    attr._arc_image->UpdateVertexBufferObjects();
}

void HyperbolicCompositeCurve3::updateNeighors(ArcAttributes &attr, Direction direction, GLuint index)
{
    if(direction == LEFT)
    {
        (*attr._arc)[0] = (*_attributes[index]._arc)[3];
        (*attr._arc)[1] = 2.0 * (*_attributes[index]._arc)[3] - (*_attributes[index]._arc)[2];
    } else
    {
        (*attr._arc)[3] = (*_attributes[index]._arc)[0];
        (*attr._arc)[2] = 2.0 * (*_attributes[index]._arc)[0] - (*_attributes[index]._arc)[1];
    }

    updateAttributesWhenArcChanged(attr);
}

void HyperbolicCompositeCurve3::modifyExistingArc(GLuint index, GLuint pointIndex, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ)
{
    std::cout << "bejott" <<std::endl;
    (*_attributes[index]._arc)[pointIndex] += DCoordinate3(shiftX, shiftY, shiftZ);


    updateAttributesWhenArcChanged(index);

    if(pointIndex >= 2)
    {
        if(_attributes[index]._next)
        {
            if(_attributes[index]._next->_next == &_attributes[index])
                updateNeighors((*_attributes[index]._next), RIGHT, index);
            else
                updateNeighors((*_attributes[index]._next), LEFT, index);
        }
    } else
    {
        if(_attributes[index]._previous)
        {
            if(_attributes[index]._previous->_next == &_attributes[index])
                updateNeighors((*_attributes[index]._previous), RIGHT, index);
            else
                updateNeighors((*_attributes[index]._previous), LEFT, index);
        }
    }

    std::cout << "vege" <<std::endl;
}

void HyperbolicCompositeCurve3::setAlpha(GLdouble alpha)
{
    _alpha = alpha;
    for(GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]._arc->setAlpha(_alpha);
        updateAttributesWhenArcChanged(_attributes[i]);
    }
}

GLuint HyperbolicCompositeCurve3::findIndex(ArcAttributes &attr)
{
    for(GLuint i = 0; i< _attributes.size(); i++)
    {
        if(&_attributes[i] == &attr) return i;
    }
}


 GLboolean HyperbolicCompositeCurve3::print(std::string filname)
 {
     std::fstream f;
     f.open(filname, std::ios::out);
     f << _alpha << std::endl;
     f << _attributes.size() << std::endl;
     for(GLuint i = 0; i < _attributes.size(); i++)
     {
         for(GLuint j = 0; j < 4; j++)
         {
            f << (*_attributes[i]._arc)[j] << " ";
         }
         f << std::endl;
     }

     for(GLuint i = 0; i < _attributes.size(); i++)
     {
         f << i << " ";
         if(_attributes[i]._previous)
         {
             f << findIndex(*_attributes[i]._previous) << " ";
         } else
         {
             f << -1 << " ";
         }
         f << std::endl;
     }

     f.close();
     return GL_TRUE;
 }

 GLuint HyperbolicCompositeCurve3::getAttributesSize()
 {
     return _attributes.size();
 }

 GLboolean HyperbolicCompositeCurve3::read(std::string filname)
 {
     std::fstream f;
     GLuint size;
     f.open(filname, std::ios::in);
     f >> _alpha;
     f >> size;
     std::cout << "size" << size << std::endl;

     for(GLuint i = 0; i < size; i++)
     {
         std::cout << "i: " << i << std::endl;
         GLuint n = (GLuint)_attributes.size();
            std::cout << "n: " << n << std::endl;
         _attributes.resize(n + 1);
         _attributes[n]._arc = new HyperbolicArc3(_alpha);

         if (!_attributes[n]._arc)
         {
             _attributes.pop_back();
             return GL_FALSE;
         }

         for(GLuint j = 0; j < 4; j++){
            f >> (*_attributes[n]._arc)[j];
            std::cout << (*_attributes[n]._arc)[j] << std::endl;}

         _attributes[n]._arc->UpdateVertexBufferObjectsOfData();

         _attributes[n]._arc_image = _attributes[n]._arc->GenerateImage(2, 30);

         if(!_attributes[n]._arc_image)
         {
             _attributes.pop_back();
             return GL_FALSE;
         }
         _attributes[n]._arc_image->UpdateVertexBufferObjects();
         _attributes[n]._color = new Color4(1.0f, 1.0f, 1.0f, 0.0f);

         _attributes[n]._next = nullptr;
         _attributes[n]._previous = nullptr;
     }

     int index;
     for(GLuint i = 0; i < size; i++)
     {
         f >> index;
         std::cout << index << std::endl;
         if(index < 0)
            _attributes[i]._previous == nullptr;
         else
             _attributes[i]._previous == &_attributes[index];

         f >> index;
         std::cout << index << std::endl;
         if(index < 0)
            _attributes[i]._next == nullptr;
         else
             _attributes[i]._next == &_attributes[index];

     }

     f.close();
     return GL_TRUE;
 }
}
