#pragma once
#include "SecondOrderHyperbolicPatch.h"
#include "../Core/TriangulatedMeshes3.h"
#include "../Core/Materials.h"
#include "../Core/ShaderPrograms.h"
#include <iostream>
#include <string>

namespace cagd {
    class HyperbolicCompositeSurface3
    {
    public:
        enum Direction{NORTH, SOUTH, EAST, WEST};
        class PatchAttributes
        {
        public:
            SecondOrderHyperbolic           *_patch;
            TriangulatedMesh3               *_images;
            Material                        *_material;
            ShaderProgram                   *_shader;
            RowMatrix<GenericCurve3*>       *_u_lines, *_v_lines;
            PatchAttributes                 *_neighbors[8];  //ezmiért kell külön?

            PatchAttributes();
            PatchAttributes(const PatchAttributes &rhs);
            PatchAttributes& operator= (const PatchAttributes& rhs);
            ~PatchAttributes();

            void setPatch(SecondOrderHyperbolic *patch);
            void setMaterial(Material &material);
//            void setMaterial();
//            void setShader();

            //constructor, copy constructor, operator=, destructro, setters/getters - ez kellene mindenre?
        };
    protected:
        std::vector<PatchAttributes>    _attributes;
        GLdouble                        _alpha;
        GLuint                          _maxPatchCapacity;

    public:
        HyperbolicCompositeSurface3(GLdouble alpha = 1.0, GLuint maxPatchCapacity = 1000);
        HyperbolicCompositeSurface3(const HyperbolicCompositeSurface3 &rhs);
        HyperbolicCompositeSurface3& operator =(const HyperbolicCompositeSurface3 &rhs);
        ~HyperbolicCompositeSurface3();

        GLboolean setAlpha(GLdouble alpha);
        GLdouble getAlpha();
        GLuint getAttributesSize();

        void insertPatch(PatchAttributes attr);
        PatchAttributes& getAtIndex(GLuint index);
        GLboolean continueExistingPatch(GLuint index, Direction direction);
        GLboolean mergeExistingPatches(GLuint firsIndex, GLuint secondIndex, Direction firstDirection, Direction secondDirection);
        GLboolean joinExistingPatches(GLuint firsIndex, GLuint secondIndex, Direction firstDirection, Direction secondDirection);

        GLboolean insertNewIsolatedDefaultPatch(
                GLuint u_div_point_count = 30, GLuint v_div_point_count = 30,
                GLuint u_iso_line_count = 2, GLuint v_iso_line_count = 2,
                GLenum usage_flag = GL_STATIC_DRAW);

        void transformPatch(GLuint index, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ);
        void updateAfterPatchChanged(GLuint index, GLuint u_div_point_count = 30, GLuint v_div_point_count = 30);
        void modifyExistingPatch(GLuint index, GLuint x, GLuint y, GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ);
        void eraseExistingPatch(GLuint index);

        void renderPatchImages();
        void renderNormalVectors();
        void renderUVLines();
        void renderUVLinesDerivatives();
        void renderPatch();
        void renderPatchColored(GLuint);
        void updateNeighborsAfterTransform(PatchAttributes &attr, std::vector<PatchAttributes*> alreadyChanged,
                                           GLdouble shiftX, GLdouble shiftY, GLdouble shiftZ);
        void mergeExistingPatchesAfterMerge(GLuint firsIndex, GLuint secondIndex,
                                            Direction firstDirection, Direction secondDirection,
                                            std::vector<PatchAttributes*> alreadyUpdated);
        GLboolean print(std::string filname);
        GLboolean read(std::string filename);
        void setMaterial(GLuint index, Material &material);

    private:
        GLuint findIndex(const PatchAttributes &attr);
    };
}
