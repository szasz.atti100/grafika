#include "../Core/Colors4.h"


using namespace cagd;

Color4 cagd::Color4Red = Color4(1.0f, 0.0f, 0.0f, 1.0f);
Color4 cagd::Color4White = Color4(1.0f, 1.0f, 1.0f, 1.0f);
Color4 cagd::Color4Yellow = Color4(1.0f, 1.0f, 0.0f, 1.0f);
Color4 cagd::Color4Green = Color4(0.0f, 1.0f, 0.0f, 1.0f);
Color4 cagd::Color4Blue = Color4(0.0f, 0.0f, 1.0f, 1.0f);

