#include "MainWindow.h"

namespace cagd
{
    MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
    {
        setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */
        _side_widget = new SideWidget(this);

        _scroll_area = new QScrollArea(this);
        _scroll_area->setWidget(_side_widget);
        _scroll_area->setSizePolicy(_side_widget->sizePolicy());
        _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        _gl_widget = new GLWidget(this);

        centralWidget()->setLayout(new QHBoxLayout());
        centralWidget()->layout()->addWidget(_gl_widget);
        centralWidget()->layout()->addWidget(_scroll_area);

        // creating a signal slot mechanism between the rendering context and the side widget
        connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));

        connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        connect(_side_widget->select_curve, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_curve_index(int)));
        connect(_side_widget->scale_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_scale(double)));
        connect(_side_widget->div_point_count_set_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_div_point_count(double)));

        connect(_side_widget->derivate_1_vector_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_derivate_1_vectors()));
        connect(_side_widget->derivate_2_vector_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_derivate_2_vectors()));

        connect(_side_widget->object_select, SIGNAL(currentChanged(int)), _gl_widget, SLOT(setObjectIndex(int)));
        connect(_side_widget->select_surface, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_surface_index(int)));

        connect(_side_widget->div_point_u_count_set_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_div_point_count_u(double)));
        connect(_side_widget->div_point_v_count_set_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_div_point_count_v(double)));

        connect(_side_widget->select_model, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_model_index(int)));

        connect(_side_widget->derivate_1_vector_checkbox_cc, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_fod_cc()));
        connect(_side_widget->derivate_2_vector_checkbox_cc, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_sod_cc()));
        connect(_side_widget->control_poligon_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_control_polygon_cc()));
        connect(_side_widget->control_interpolating_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_interpolating_cc()));
        connect(_side_widget->shader_checkBox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_shader()));
        connect(_side_widget->shader_index_comboBox, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_shader_index(int)));
        connect(_side_widget->control_poligon_checkbox_patch, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_control_polygon_patch()));
        connect(_side_widget->isoparametric_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_isoparametric_lines()));
        connect(_side_widget->inerpilated_patch_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_show_interpolating_patch()));
        connect(_side_widget->animation_checkbox, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_animation()));
        connect(_side_widget->v_lines_spinBox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_v_lines_count(int)));
        connect(_side_widget->u_lines_spinBox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_u_lines_count(int)));

        connect(_side_widget->curve_direction_1, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_curve_direction_1(int)));
        connect(_side_widget->curve_index_1,SIGNAL(valueChanged(int)),_gl_widget,SLOT(set_curve_index_1(int)));
        connect(_side_widget->curve_direction_2, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_curve_direction_2(int)));
        connect(_side_widget->curve_index_2,SIGNAL(valueChanged(int)),_gl_widget,SLOT(set_curve_index_2(int)));
//insert
        connect(_side_widget->curve_insert_button,SIGNAL(clicked(bool)),_gl_widget,SLOT(curve_insert_button_pressed(bool)));
//shift curve
        connect(_side_widget->shift_curve_x,SIGNAL(textChanged(QString)),_gl_widget,SLOT(set_curve_shift_x(QString)));
        connect(_side_widget->shift_curve_y,SIGNAL(textChanged(QString)),_gl_widget,SLOT(set_curve_shift_y(QString)));
        connect(_side_widget->shift_curve_z,SIGNAL(textChanged(QString)),_gl_widget,SLOT(set_curve_shift_z(QString)));
        connect(_side_widget->curve_shift_button,SIGNAL(clicked(bool)),_gl_widget,SLOT(curve_shift_button_pressed(bool)));


        connect(_gl_widget,SIGNAL(curve_insert_pressed(int)),_side_widget,SLOT(set_max_curve_value(int))); //itt!!!

//continue
        connect(_side_widget->curve_continue_button,SIGNAL(clicked(bool)),_gl_widget,SLOT(curve_continue_button_pressed(bool)));

//merge
        connect(_side_widget->curve_merge_button,SIGNAL(clicked(bool)),_gl_widget,SLOT(curve_merge_button_pressed(bool)));
//join
        connect(_side_widget->curve_join_button,SIGNAL(clicked(bool)),_gl_widget,SLOT(curve_join_button_pressed(bool)));
//delete
        connect(_side_widget->curve_delete_button,SIGNAL(clicked(bool)),_gl_widget,SLOT(curve_delete_button_pressed(bool)));
    }

    //--------------------------------
    // implementation of private slots
    //--------------------------------
    void MainWindow::on_action_Quit_triggered()
    {
        qApp->exit(0);
    }
}
