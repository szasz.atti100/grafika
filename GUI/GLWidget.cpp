#include "GLWidget.h"

#if !defined(__APPLE__)
#include <GL/glu.h>
#endif

#include <iostream>
using namespace std;

#include <Core/Exceptions.h>
#include "../Core/Matrices.h"
#include "../Test/TestFunctions.h"
#include "../Parametric/ParametricCurves3.h"

namespace cagd
{
    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent, const QGLFormat &format): QGLWidget(format, parent)
    {
        _timer = new QTimer(this);
        _timer->setInterval(0);

        connect(_timer, SIGNAL(timeout()), this, SLOT(_animate()));
    }

    //--------------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------

    GLWidget::~GLWidget()
    {
        for (GLuint i = 0; i < _pc.GetColumnCount(); i++)
        {
            if (_pc[i])
                delete _pc[i], _pc[i] = nullptr;
            if (_image_of_pc[i])
                delete _image_of_pc[i], _image_of_pc[i] = nullptr;
        }
    }


    void GLWidget::initializeGL()
    {
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = (double)width() / (double)height();
        _z_near = 1.0;
        _z_far  = 1000.0;
        _fovy   = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

                // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

                // enabling the depth test
        glEnable(GL_DEPTH_TEST);

                // setting the background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

                // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;

        try{

            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                        "Try to update your driver or buy a new graphics adapter!");
            }

            glEnable(GL_POINT_SMOOTH);
            glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
            glEnable(GL_LINE_SMOOTH);
            glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
            glEnable(GL_POLYGON_SMOOTH);
            glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

            glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

            glEnable(GL_DEPTH_TEST);

            glewInit();

//            RowMatrix<ParametricCurve3::Derivative> derivative(3);
//            derivative(0) = vasque::d0;
//            derivative(1) = vasque::d1;
//            derivative(2) = vasque::d2;

            initializeCurves();
            initializeSurfaces();
            initializeModels();
            initCyclicCurves();
            initPatches();
            initShaders();
            initCurve();

        }
        catch (Exception &e)
        {
            cout << e << endl;
        }
    }

    //-----------------------
    // the rendering function
    //-----------------------
    void GLWidget::paintGL()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glPushMatrix();

        glRotatef(_angle_x, 1.0, 0.0, 0.0);
        glRotatef(_angle_y, 0.0, 1.0, 0.0);
        glRotatef(_angle_z, 0.0, 0.0, 1.0);
        glTranslated(_trans_x, _trans_y, _trans_z);
        glScaled(_zoom, _zoom, _zoom);

        if(_show_shader)
        {
            _shaders[_shader_index].Enable();
        }

//        switch (_object_index) {
//        case 0:
//            drawParametricCurves();
//            break;
//        case 1:
//            drawSurfaces();
//            break;
//        case 2:
//            drawModels();
//            break;
//        case 3:
//            drawCyclicCurve();
//            break;
//        case 4:
//            drawPatches();
//            break;
//        default:
//            break;
//        }

        drawCurves();

        if (_show_shader)
        {
            _shaders[_shader_index].Disable();
        }

        glPopMatrix();

    }

    void GLWidget::initCurve()
    {
        _compositeCurve = nullptr;
        _compositeCurve = new HyperbolicCompositeCurve3(1.0);
    }

    void GLWidget::drawCurves()
    {
        _compositeCurve->setColor(_curve_index_1, new Color4(1.0f,0.0f,0.0f,1.0f));
        _compositeCurve->setColor(_curve_index_2, new Color4(0.0f,0.0f,1.0f,1.0f));

        _compositeCurve->renderImage();
//        _compositeCurve->renderFirstOrderDerivatives();
        _compositeCurve->renderControlPoint();
    }

    void GLWidget::initShaders()
    {
        _shader_index = 0;
        _show_shader = false;
        _shaders.ResizeColumns(4);
        _default_outline_color.ResizeColumns(3);
        for(GLuint i = 0; i < 3; i++) {
            _default_outline_color[i] = 0.5f;
        }

        try
        {
            if(!_shaders[0].InstallShaders("Shaders/directional_light.vert", "Shaders/directional_light.frag"))
            {
                throw Exception("Directional light failed to load.");
            }

            if(!_shaders[1].InstallShaders("Shaders/reflection_lines.vert", "Shaders/reflection_lines.frag"))
            {
                throw Exception("Reflection lines failed to load.");
            } else {
                _shaders[1].Enable();
                _shaders[1].SetUniformVariable1f("scale_factor", 4.0f);
                _shaders[1].SetUniformVariable1f("smoothing", 2.0f);
                _shaders[1].SetUniformVariable1f("shading", 1.0f);
                _shaders[1].Disable();
            }

            if(!_shaders[2].InstallShaders("Shaders/toon.vert", "Shaders/toon.frag"))
            {
                throw Exception("Toon failed to load.");
            }
                _shaders[2].Enable();
                _shaders[2].SetUniformVariable4f("default_outline_color", 0.5f, 0.5f, 0.5f, 1.0f);
                _shaders[2].Disable();

            if(!_shaders[3].InstallShaders("Shaders/two_sided_lighting.vert", "Shaders/two_sided_lighting.frag"))
            {
                throw Exception("Two sided lighting failed to load.");
            }
        }
        catch(Exception &e)
        {
            cerr << e << endl;
        }
    }

    void GLWidget::initIsometricLines()
    {
        _u_lines = _patch.GenerateUIsoparametricLines(_u_lines_count, 1, 30);
        _v_lines = _patch.GenerateVIsoparametricLines(_v_lines_count, 1, 30);

        for(GLuint i = 0; i < _u_lines->GetColumnCount(); i++)
        {

            if((*_u_lines)[i])
                {
                    (*_u_lines)[i]->UpdateVertexBufferObjects();
                }
        }

        for(GLuint i = 0; i < _v_lines->GetColumnCount(); i++)
        {
            if((*_v_lines)[i])
            {
                (*_v_lines)[i]->UpdateVertexBufferObjects();
            }
        }
    }

    void GLWidget::initPatches()
    {
        _patch.SetData(0, 0, -2.0, -2.0, 0.0);
        _patch.SetData(0, 1, -2.0, -1.0, 0.0);
        _patch.SetData(0, 2, -2.0, 1.0, 0.0);
        _patch.SetData(0, 3, -2.0, 2.0, 0.0);

        _patch.SetData(1, 0, -1.0, -2.0, 0.0);
        _patch.SetData(1, 1, -1.0, -1.0, 2.0);
        _patch.SetData(1, 2, -1.0, 1.0, 2.0);
        _patch.SetData(1, 3, -1.0, 2.0, 0.0);

        _patch.SetData(2, 0, 1.0, -2.0, 0.0);
        _patch.SetData(2, 1, 1.0, -1.0, 2.0);
        _patch.SetData(2, 2, 1.0, 1.0, 2.0);
        _patch.SetData(2, 3, 1.0, 2.0, 0.0);

        _patch.SetData(3, 0, 2.0, -2.0, 0.0);
        _patch.SetData(3, 1, 2.0, -1.0, 0.0);
        _patch.SetData(3, 2, 2.0, 1.0, 0.0);
        _patch.SetData(3, 3, 2.0, 2.0, 0.0);

        _show_control_polygon_patch = false;

        initIsometricLines();

        _patch.UpdateVertexBufferObjectsOfData();

        _before_interpolation = _patch.GenerateImage(30, 30, GL_STATIC_DRAW);

        if(_before_interpolation)
        {
            _before_interpolation->UpdateVertexBufferObjects();
        }

        RowMatrix<GLdouble> u_knot_vector(4);
        u_knot_vector(0) = 0.0;
        u_knot_vector(1) = 1.0 / 3.0;
        u_knot_vector(2) = 2.0 / 3.0;
        u_knot_vector(3) = 1.0;

        ColumnMatrix<GLdouble> v_knot_vector(4);
        v_knot_vector(0) = 0.0;
        v_knot_vector(1) = 1.0 / 3.0;
        v_knot_vector(2) = 2.0 / 3.0;
        v_knot_vector(3) = 1.0;

        Matrix<DCoordinate3> data_points_to_interpolate(4, 4);
        for(GLuint row = 0; row < 4; ++row)
        {
            for(GLuint column = 0; column < 4; ++column)
            {
                _patch.GetData(row, column, data_points_to_interpolate(row, column));
            }
        }

        if(_patch.UpdateDataForInterpolation(u_knot_vector, v_knot_vector, data_points_to_interpolate))
        {
            _after_interpolation = _patch.GenerateImage(30, 30, GL_STATIC_DRAW);

            if(_after_interpolation)
            {
                _after_interpolation->UpdateVertexBufferObjects();
            }
        }
    }

    void GLWidget::drawPatches()
    {
        glColor3f(1.0f, 1.0f, 1.0f);
        if (_show_control_polygon_patch)
        {
            _patch.RenderData();
        }

        if (_show_isoparametric_lines)
        {
            for(GLuint i = 0; i < _u_lines->GetColumnCount(); i++)
            {
                if((*_u_lines)[i])
                {
                    glColor3f(1.0f, 1.0f, 0.0f);
                    (*_u_lines)[i]->RenderDerivatives(0, GL_LINE_STRIP);
                }
            }

            for(GLuint i = 0; i < _v_lines->GetColumnCount(); i++)
            {
                if((*_v_lines)[i])
                {
                    glColor3f(1.0f, 0.0f, .0f);
                    (*_v_lines)[i]->RenderDerivatives(0, GL_LINE_STRIP);
                }
            }
        }

        glEnable(GL_LIGHTING);
        if(_before_interpolation)
        {
            _before_interpolation->Render();
        }

        if (_show_interpolating_patch)
        {
            if(_after_interpolation)
            {
                glEnable(GL_BLEND);
                glDepthMask(GL_FALSE);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                _after_interpolation->Render();
                glDepthMask(GL_TRUE);
                glDisable(GL_BLEND);
            }
        }
        glDisable(GL_LIGHTING);
    }

    void GLWidget::drawSurfaces()
    {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);
        if(_image_of_ps[_surface_index]){
            _image_of_ps[_surface_index]->Render();
        }
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_NORMALIZE);
    }

    void GLWidget::drawModels()
    {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);
        switch(_model_index)
        {
            case 0:
                _models[0].Render();
                break;
            case 1:
                _models[1].Render();
                break;
            case 2:
                _models[2].Render();
                break;
        }
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_NORMALIZE);
    }

    void GLWidget::wheelEvent(QWheelEvent *event)
    {
        if (event->delta() > 0)
        {
            _zoom *= 1.1;
            updateGL();
        }
        else if (event->delta() < 0)
        {
            _zoom *= 0.9;
            updateGL();
        }
    }

    void GLWidget::drawParametricCurves()
    {
        if (_image_of_pc[_curve_index])
        {
            glColor3f(1.0f, 0.0f, 0.0f);
            _image_of_pc[_curve_index]->RenderDerivatives(0, GL_LINE_STRIP);

            glPointSize(5.0f);

            if (derivate_1_vectors)
            {
                glColor3f(0.0f, 0.0f, 0.5f);
                _image_of_pc[_curve_index]->RenderDerivatives(1, GL_LINES);
                _image_of_pc[_curve_index]->RenderDerivatives(1, GL_POINTS);
            }
            if (derivate_2_vectors)
            {
                glColor3f(0.0f, 0.5f, 0.0f);
                _image_of_pc[_curve_index]->RenderDerivatives(2, GL_LINES);
                _image_of_pc[_curve_index]->RenderDerivatives(2, GL_POINTS);
            }

            glPointSize(1.0f);
        }
    }

    void GLWidget::initCyclicCurves()
    {
        _cc = nullptr;
        _cc_i = nullptr;
        _n = 3;
        _cc = new CyclicCurve3(_n);
        _cc_i = new CyclicCurve3(_n);

        _knot_vector.ResizeRows(2 * _n + 1);
        _data_points_to_interpolate.ResizeRows(2 * _n + 1);

        if(!_cc)
        {
            throw Exception("Could not create the cyclic curve!");
        }
        if(!_cc_i)
        {
            throw Exception("Could not create the interpolation for the cyclic curve!");
        }

        GLdouble step = TWO_PI/(2 * _n + 1);
        for(GLuint i = 0; i <= 2 * _n; i++)
        {
            GLdouble u = i * step;
            _knot_vector[i] = u;
            DCoordinate3 &cp = (*_cc)[i];
            cp[0] = cos(u);
            cp[1] = sin(u);
            cp[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
            _data_points_to_interpolate[i] = cp;
        }

        //interpolating cyclic curve
        if(!_cc_i->UpdateDataForInterpolation(_knot_vector, _data_points_to_interpolate))
        {
            if(_cc_i)
            {
                delete _cc_i, _cc_i = 0;
            }
            throw Exception("Could not update the VBOs of the cyclic curve's interpolation!");
        }

        if(!_cc_i->UpdateVertexBufferObjectsOfData())
        {
            if(_cc_i)
            {
                delete _cc_i, _cc_i = 0;
            }
            throw Exception("Could not update the VBOs of the cyclic curve's control polygon!-interpolation");
        }

        _div = 200;
        _mod = 3;
        _image_of_cc_i = _cc_i->GenerateImage(_mod, _div);

//        cout << *_image_of_cc << endl;

        if(!_image_of_cc_i)
        {
            if(_cc_i)
            {
                delete _cc_i, _cc_i = 0;
            }
            throw Exception("Could not generate the image of the cyclic curve!-interpolation");
        }

        if (!_image_of_cc_i->UpdateVertexBufferObjects())
        {
            if(_cc_i)
            {
                delete _cc_i, _cc_i = 0;
            }
            throw Exception("Could not update the VBOs of the cyclic curve's image!-inetpolataion");
        }

        //simple cyclic curve
        _div = 200;
        _mod = 3;
        _image_of_cc = _cc->GenerateImage(_mod, _div);

        if(!_image_of_cc)
        {
            if(_cc)
            {
                delete _cc, _cc = 0;
            }
            throw Exception("Could not generate the image of the cyclic curve!");
        }

        if(!_cc->UpdateVertexBufferObjectsOfData())
        {
            if(_cc)
            {
                delete _cc, _cc = 0;
            }
            throw Exception("Could not update the VBOs of the cyclic curve's control polygon!");
        }

        if (!_image_of_cc->UpdateVertexBufferObjects())
        {
            if(_cc)
            {
                delete _cc, _cc = 0;
            }
            throw Exception("Could not update the VBOs of the cyclic curve's image!");
        }
    }

    void GLWidget::drawCyclicCurve(){
        if (_cc) {
            if (_show_control_polygon_cc) {
                glColor3f(1.0f, 0.7f, 0.4f);
                _cc->RenderData(GL_LINE_LOOP);
                _cc->RenderData(GL_POINTS);
            }
        }

        if (_image_of_cc_i) {
            if(_show_interpolating_cc){
                glColor3f(0.5f, 1.0f, 1.0f);
                _image_of_cc_i->RenderDerivatives(0, GL_LINE_LOOP);
            }
        }

        if(_image_of_cc){
            glColor3f(1.0f, 0.0f, 0.0f);
            _image_of_cc->RenderDerivatives(0, GL_LINE_LOOP);

            glPointSize(5.0);

            if(_show_fod_cc){
                glColor3f(0.0f, 0.5f, 0.0f);
                _image_of_cc->RenderDerivatives(1, GL_LINES);
                _image_of_cc->RenderDerivatives(1, GL_POINTS);
            }

            if(_show_sod_cc){
                glColor3f(0.1f, 0.5f, 0.9f);
                _image_of_cc->RenderDerivatives(2, GL_LINES);
                _image_of_cc->RenderDerivatives(2, GL_POINTS);
            }

        glPointSize(1.0);
        }
    }

    void GLWidget::initializeCurves()
    {
        ColumnMatrix< RowMatrix<ParametricCurve3::Derivative> > derivative(5);
        ColumnMatrix< RowMatrix<GLdouble> > limits(5);

        for (GLuint i = 0; i < 5; ++i)
        {
            derivative[i].ResizeColumns(3);
            limits[i].ResizeColumns(2);
        }

        derivative(0)(0) = spiral_on_cone::d0;
        derivative(0)(1) = spiral_on_cone::d1;
        derivative(0)(2) = spiral_on_cone::d2;

        limits[0][0] = spiral_on_cone::u_min;
        limits[0][1] = spiral_on_cone::u_max;

        derivative(1)(0) = torus_knot::d0;
        derivative(1)(1) = torus_knot::d1;
        derivative(1)(2) = torus_knot::d2;

        limits[1][0] = spiral_on_cone::u_min;
        limits[1][1] = spiral_on_cone::u_max;

        derivative(2)(0) = cylinder_spiral::d0;
        derivative(2)(1) = cylinder_spiral::d1;
        derivative(2)(2) = cylinder_spiral::d2;

        limits[2][0] = spiral_on_cone::u_min;
        limits[2][1] = spiral_on_cone::u_max;

        derivative(3)(0) = lissajous::d0;
        derivative(3)(1) = lissajous::d1;
        derivative(3)(2) = lissajous::d2;

        limits[3][0] = spiral_on_cone::u_min;
        limits[3][1] = spiral_on_cone::u_max;

        derivative(4)(0) = vasque::d0;
        derivative(4)(1) = vasque::d1;
        derivative(4)(2) = vasque::d2;

        limits[4][0] = spiral_on_cone::u_min;
        limits[4][1] = spiral_on_cone::u_max;

        _pc.ResizeColumns(5);
        _image_of_pc.ResizeColumns(5);

        GLenum usage_flag = GL_STATIC_DRAW;
        for (GLuint i = 0; i < derivative.GetRowCount(); i++)
        {
            _pc[i] = nullptr;
            _pc[i] = new (nothrow) ParametricCurve3(derivative(i), limits[i][0], limits[i][1]);

            if (!_pc[i])
            {
                cout << "error!";
            }

            _image_of_pc[i] = _pc[i]->GenerateImage(div_point_count, usage_flag);

            if(!_image_of_pc[i])
            {
                cout << "error";
            }

            if (!_image_of_pc[i]->UpdateVertexBufferObjects(_scale, usage_flag))
            {
                cout << "error!";
            }
        }
    }

    void GLWidget::initializeModels()
    {
        _models.ResizeColumns(3);
        _models[0].LoadFromOFF("Models/mouse.off", true);
        _models[1].LoadFromOFF("Models/elephant.off", true);
        _models[2].LoadFromOFF("Models/sphere.off", true);

        for(int i = 0; i < _models.GetColumnCount(); i++)
        {
            _models[i].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW);
        }

        _angle = 0.0;
    }

    void GLWidget::initializeSurfaces()
    {
        TriangularMatrix<ParametricSurface3::PartialDerivative> derivative(3);
                _ps.ResizeColumns(5);
                _image_of_ps.ResizeColumns(5);
                _surface_index = 0;

                derivative(0,0) = torus::d00;
                derivative(1,0) = torus::d10;
                derivative(1,1) = torus::d01;
                _ps[0] = new ParametricSurface3(derivative, torus::u_min, torus::u_max,
                                                torus::v_min, torus::v_max);

                derivative(0,0) = hyperboloid::d00;
                derivative(1,0) = hyperboloid::d10;
                derivative(1,1) = hyperboloid::d01;
                _ps[1] = new ParametricSurface3(derivative, hyperboloid::u_min, hyperboloid::u_max,
                                                hyperboloid::v_min, hyperboloid::v_max);

                derivative(0,0) = cylindrical_helicoid::d00;
                derivative(1,0) = cylindrical_helicoid::d10;
                derivative(1,1) = cylindrical_helicoid::d01;
                _ps[2] = new ParametricSurface3(derivative, cylindrical_helicoid::u_min, cylindrical_helicoid::u_max,
                                                cylindrical_helicoid::v_min, cylindrical_helicoid::v_max);

                derivative(0,0) = alfred_gray::d00;
                derivative(1,0) = alfred_gray::d10;
                derivative(1,1) = alfred_gray::d01;
                _ps[3] = new ParametricSurface3(derivative, alfred_gray::u_min, alfred_gray::u_max,
                                                alfred_gray::v_min, alfred_gray::v_max);

                derivative(0,0) = klein_bottle::d00;
                derivative(1,0) = klein_bottle::d10;
                derivative(1,1) = klein_bottle::d01;
                _ps[4] = new ParametricSurface3(derivative, klein_bottle::u_min, klein_bottle::u_max,
                                                klein_bottle::v_min, klein_bottle::v_max);

                for(GLuint i = 0; i < 5; i++)
                {
                    if(!_ps[i])
                    {
                        throw Exception("Couldn't create the parameteric surface.");
                    }

                    GLenum usage_flag = GL_STATIC_DRAW;

                    _image_of_ps[i] = 0;
                    _image_of_ps[i] = _ps[i]->GenerateImage(div_point_count_u, div_point_count_v, usage_flag);

                    if(!_image_of_ps[i])
                    {
                        if(_ps[i])
                        {
                            delete _ps[i], _ps[i] = 0;
                        }
                        throw Exception("Couldn't create the parametric surface's image.");
                    }

                    if(!_image_of_ps[i]->UpdateVertexBufferObjects(usage_flag)){
                        cout << "could not create the vertex buffer object of the parametric surface!" << endl;
                    }
                }
    }

    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = (double)w / (double)h;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        updateGL();
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            updateGL();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            updateGL();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            updateGL();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            updateGL();
        }
    }

    void GLWidget::set_show_fod()
    {
        _show_fod = !_show_fod;
        updateGL();
    }
    void GLWidget::set_show_sod()
    {
        _show_sod = !_show_sod;
        updateGL();
    }
    void GLWidget::set_show_fod_cc()
    {
        _show_fod_cc = !_show_fod_cc;
        updateGL();
    }
    void GLWidget::set_show_sod_cc()
    {
        _show_sod_cc = !_show_sod_cc;
        updateGL();
    }
    void GLWidget::set_show_control_polygon_cc()
    {
        _show_control_polygon_cc = !_show_control_polygon_cc;
        updateGL();
    }
    void GLWidget::set_show_interpolating_cc()
    {
        _show_interpolating_cc = !_show_interpolating_cc;
        updateGL();
    }

    void GLWidget::set_show_shader()
    {
        _show_shader = !_show_shader;
        updateGL();
    }

    void GLWidget::set_div_point_count(double value)
    {
        if (div_point_count != value)
        {
            div_point_count = value;

            GLenum usage_flag = GL_STATIC_DRAW;

            for (GLuint i = 0; i < _pc.GetColumnCount(); i++)
            {
                _image_of_pc[i] = _pc[i]->GenerateImage(div_point_count, usage_flag);

                if(!_image_of_pc[i])
                {
                    cout << "error";
                }

                if (!_image_of_pc[i]->UpdateVertexBufferObjects(_scale, usage_flag))
                {
                    cout << "error!";
                }
            }
            updateGL();
        }
    }
    void GLWidget::set_div_point_count_u(double value)
    {
        if(div_point_count_u != value){
            div_point_count_u = value;
            for(GLuint i = 0; i < 5; i++)
            {
                if(!_ps[i])
                {
                    throw Exception("Couldn't create the parameteric surface.");
                }

                GLenum usage_flag = GL_STATIC_DRAW;

                _image_of_ps[i] = 0;
                _image_of_ps[i] = _ps[i]->GenerateImage(div_point_count_u, div_point_count_v, usage_flag);

                if(!_image_of_ps[i])
                {
                    if(_ps[i])
                    {
                        delete _ps[i], _ps[i] = 0;
                    }
                    throw Exception("Couldn't create the parametric surface's image.");
                }

                if(!_image_of_ps[i]->UpdateVertexBufferObjects(usage_flag)){
                    cout << "could not create the vertex buffer object of the parametric surface!" << endl;
                }
            }
        }
        updateGL();
    }

    void GLWidget::set_div_point_count_v(double value)
    {
        if(div_point_count_v != value){
            div_point_count_v = value;
            for(GLuint i = 0; i < 5; i++)
            {
                if(!_ps[i])
                {
                    throw Exception("Couldn't create the parameteric surface.");
                }

                GLenum usage_flag = GL_STATIC_DRAW;

                _image_of_ps[i] = 0;
                _image_of_ps[i] = _ps[i]->GenerateImage(div_point_count_u, div_point_count_v, usage_flag);

                if(!_image_of_ps[i])
                {
                    if(_ps[i])
                    {
                        delete _ps[i], _ps[i] = 0;
                    }
                    throw Exception("Couldn't create the parametric surface's image.");
                }

                if(!_image_of_ps[i]->UpdateVertexBufferObjects(usage_flag)){
                    cout << "could not create the vertex buffer object of the parametric surface!" << endl;
                }
            }
        }
        updateGL();
    }

    void GLWidget::set_curve_index(int ind)
    {
        if (_curve_index != ind)
        {
            _curve_index = ind;
            updateGL();
        }
    }

    void GLWidget::set_model_index(int index)
    {
        if(_model_index != index)
        {
            _model_index = index;
            updateGL();
        }
    }

    void GLWidget::set_surface_index(int ind)
    {
        if (_surface_index != ind)
        {
            _surface_index = ind;
            updateGL();
        }
    }

    void GLWidget::setObjectIndex(int ind)
    {
        if (_object_index != ind)
        {
            _object_index = ind;
            updateGL();
        }
    }

    void GLWidget::set_shader_index(int ind)
    {
        if (_shader_index != ind)
        {
            _shader_index = ind;
            updateGL();
        }
    }

    void GLWidget::set_scale(double new_scale)
    {
        if (_scale != new_scale)
        {
            _scale = new_scale;

            GLenum usage_flag = GL_STATIC_DRAW;

            for (GLuint i = 0; i < _pc.GetColumnCount(); i++)
                if (!_image_of_pc[i]->UpdateVertexBufferObjects(_scale, usage_flag))
                {
                    cout << "error!";
                }

            _image_of_cc->UpdateVertexBufferObjects(_scale);

            updateGL();
        }
    }

    void GLWidget::set_scale_surface(double new_scale)
    {
        if (_scale_surface != new_scale)
        {
            _scale_surface = new_scale;

            GLenum usage_flag = GL_STATIC_DRAW;

            for (GLuint i = 0; i < _ps.GetColumnCount(); i++)
                if (!_image_of_ps[i]->UpdateVertexBufferObjects(usage_flag))
                {
                    cout << "error!";
                }

            updateGL();
        }
    }

    void GLWidget::set_derivate_1_vectors()
    {
        derivate_1_vectors = !derivate_1_vectors;
        updateGL();
    }
    void GLWidget::set_derivate_2_vectors()
    {
        derivate_2_vectors = !derivate_2_vectors;
        updateGL();
    }

    void GLWidget::set_show_control_polygon_patch()
    {
        _show_control_polygon_patch = !_show_control_polygon_patch;
        updateGL();
    }

    void GLWidget::set_show_interpolating_patch()
    {
        _show_interpolating_patch = !_show_interpolating_patch;
        updateGL();
    }

    void GLWidget::set_show_isoparametric_lines()
    {
        _show_isoparametric_lines = !_show_isoparametric_lines;
        updateGL();
    }

    void GLWidget::_animate()
    {
        GLfloat *vertex = _models[_model_index].MapVertexBuffer(GL_READ_WRITE);
        GLfloat *normal = _models[_model_index].MapNormalBuffer(GL_READ_ONLY);

        _angle += DEG_TO_RADIAN;

        if(_angle >= TWO_PI)
        {
        _angle -= TWO_PI;
        }

        GLfloat scale = sin(_angle) / 3000.0;
        for(GLuint i = 0; i < _models[_model_index].VertexCount(); i++)
        {
            for(GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex, ++normal)
            {
                *vertex += scale * (*normal);
            }
        }

        _models[_model_index].UnmapNormalBuffer();
        _models[_model_index].UnmapVertexBuffer();
        updateGL();
    }

    void GLWidget::set_animation()
    {
        if (_timer->isActive())
        {
            _timer->stop();
        }
        else
        {
            _timer->start();
        }
    }

    void GLWidget::set_u_lines_count(int count)
    {
        if(_u_lines_count != count)
        {
            _u_lines_count = count;
            initIsometricLines();
            updateGL();
        }
    }

    void GLWidget::set_v_lines_count(int count)
    {
        if(_v_lines_count != count)
        {
            _v_lines_count = count;
            initIsometricLines();
            updateGL();
        }
    }

    void GLWidget::curve_insert_button_pressed(bool value)
    {
        _curve_insert++;
        emit curve_insert_pressed(_curve_insert);
        _compositeCurve->insertDefaultArc();
        updateGL();
    }

    void GLWidget::set_curve_index_1(int value)
    {
        if(_curve_index_1 != value){
            _compositeCurve->setColor(_curve_index_1, new Color4(1.0f,1.0f,1.0f,1.0f));
            _curve_index_1 = value;
            updateGL();
        }
    }

    void GLWidget::set_curve_index_2(int value)
    {
        if(_curve_index_2 != value){
            _compositeCurve->setColor(_curve_index_2, new Color4(1.0f,1.0f,1.0f,1.0f));
            _curve_index_2 = value;
            updateGL();
        }
    }

    void GLWidget::set_curve_shift_x(QString value)
    {
        if(_curve_shift_x != value.toDouble())
        {
            _curve_shift_x = value.toDouble();
        }
    }

    void GLWidget::set_curve_shift_y(QString value)
    {
        if(_curve_shift_y != value.toDouble())
        {
            _curve_shift_y = value.toDouble();
        }
    }

    void GLWidget::set_curve_shift_z(QString value)
    {
        if(_curve_shift_z != value.toDouble())
        {
            _curve_shift_z = value.toDouble();
        }
    }

    void GLWidget::curve_shift_button_pressed(bool)
    {
        _compositeCurve->shiftExistingArc(_curve_index_1, _curve_shift_x, _curve_shift_y, _curve_shift_z);
        updateGL();
    }

    void GLWidget::set_curve_direction_1(int value)
    {
        if(_curve_direction_1 != value)
        {
            _curve_direction_1 = value;
        }
    }

    void GLWidget::set_curve_direction_2(int value)
    {
        if(_curve_direction_2 != value)
        {
            _curve_direction_2 = value;
        }
    }

    void GLWidget::curve_merge_button_pressed(bool)
    {
        if( _compositeCurve->mergeExistingArcs(_curve_index_1, _curve_index_2, HyperbolicCompositeCurve3::Direction(_curve_direction_1), HyperbolicCompositeCurve3::Direction(_curve_direction_2)) )
        {
            updateGL();
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Nem lehet osszemosni");
            msgBox.exec();
        }
    }

    void GLWidget::curve_join_button_pressed(bool)
    {
        if( _compositeCurve->joinExistingArcs(_curve_index_1, _curve_index_2, HyperbolicCompositeCurve3::Direction(_curve_direction_1), HyperbolicCompositeCurve3::Direction(_curve_direction_2)) )
        {
            updateGL();
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Nem lehet osszefuzni");
            msgBox.exec();
        }
    }

    void GLWidget::curve_delete_button_pressed(bool)
    {
        _curve_insert--;
        if( _compositeCurve->eraseExistingArc(_curve_index_1) )
        {
            updateGL();
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Nem lehet torolni");
            msgBox.exec();
        }
    }

    void GLWidget::curve_continue_button_pressed(bool)
    {
        if (_compositeCurve->continueExistingArc(_curve_index_1, HyperbolicCompositeCurve3::Direction(_curve_direction_1)))
        {
            _curve_insert++;
            emit curve_insert_pressed(_curve_insert);
            updateGL();
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Nem lehet folytatni");
            msgBox.exec();
        }
    }
}
