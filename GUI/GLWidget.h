#pragma once

#include <GL/glew.h>
#include <QGLWidget>
#include <QMessageBox>
#include "../Parametric/ParametricCurves3.h"
#include "../Core/TriangulatedMeshes3.h"
#include "../Cyclic/CyclicCurve3.h"
#include "../Parametric/ParametricSurfaces3.h"
#include "../Core/Constants.h"
#include "../Hyperbolic/hyperbolicarcs3.h"
#include "../Hyperbolic/hyperboliccompositecurves3.h"
#include "../Hyperbolic/SecondOrderHyperbolicPatch.h"
#include "../Core/ShaderPrograms.h"
#include <QWheelEvent>
#include <QTimer>

namespace cagd
{
    class GLWidget: public QGLWidget
    {
        Q_OBJECT

    private:

        // variables defining the projection matrix
        double       _aspect;            // aspect ratio of the rendering window
        double       _fovy;              // field of view in direction y
        double       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        double       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;

        // your other declarations

        GLuint _object_index = 0;

        RowMatrix<ParametricCurve3*> _pc;
        RowMatrix<GenericCurve3*> _image_of_pc;
        GLdouble _scale = 1;

        GLuint div_point_count = 200;

        GLuint _curve_index = 0;

        GLboolean derivate_1_vectors = GL_FALSE;
        GLboolean derivate_2_vectors = GL_FALSE;

        RowMatrix<ParametricSurface3*> _ps;
        RowMatrix<TriangulatedMesh3*> _image_of_ps;
        GLdouble _scale_surface = 1;

        GLuint div_point_count_u = 200;
        GLuint div_point_count_v = 200;

        GLuint _surface_index = 0;

        GLuint _model_index = 0;

        RowMatrix<TriangulatedMesh3> _models;

        QTimer*             _timer;
        GLfloat             _angle;

        CyclicCurve3*                   _cc;
        CyclicCurve3*                   _cc_i;
        GLuint                          _n; //order of the cyclic curce
        bool                       _show_fod = false, _show_sod = false, _show_fod_cc = false, _show_sod_cc = false;
        bool                       _show_control_polygon_cc = false, _show_interpolating_cc = false;
        ColumnMatrix<GLdouble>          _knot_vector;
        ColumnMatrix<DCoordinate3>      _data_points_to_interpolate;
        GLuint                          _div; //number of subdivision points
        GLuint                          _mod; //maximum order of derivatives
        GenericCurve3*                  _image_of_cc;
        GenericCurve3*                  _image_of_cc_i;

        SecondOrderHyperbolic       _patch;
        RowMatrix<GenericCurve3*>*  _u_lines, *_v_lines;
        GLuint                      _u_lines_count = 10, _v_lines_count = 10;
        TriangulatedMesh3       *_before_interpolation, *_after_interpolation;

        ShaderProgram   _shader;
        RowMatrix<ShaderProgram>    _shaders;
        GLint                       _shader_index;
        bool                        _show_shader, _show_control_polygon_patch, _show_interpolating_patch, _show_isoparametric_lines;
        double                      _scale_factor, _smoothing, _shading;
        RowMatrix<float>            _default_outline_color;

        // curve

        GLuint                      _curve_index_1 = 0;
        GLuint                      _curve_direction_1 = 0;
        GLuint                      _curve_index_2 = 0;
        GLuint                      _curve_direction_2 = 0;
        GLdouble                    _curve_shift_x;
        GLdouble                    _curve_shift_y;
        GLdouble                    _curve_shift_z;

        HyperbolicCompositeCurve3   *_compositeCurve;
        GLuint                      _curve_insert = 0;

        void wheelEvent(QWheelEvent *);

    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = 0, const QGLFormat& format = QGL::Rgba | QGL::DepthBuffer | QGL::DoubleBuffer);

        // redeclared virtual functions

        void initializeCurves();
        void initializeSurfaces();
        void initializeModels();
        void initCyclicCurves();
        void initPatches();
        void initIsometricLines();
        void initShaders();
        void drawParametricCurves();
        void drawModels();
        void drawSurfaces();
        void drawCyclicCurve();
        void drawPatches();
        void initCurve();
        void drawCurves();



        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);

        virtual ~GLWidget();

    private slots:
        void _animate();

    public slots:
        // public event handling methods/slots
        void set_curve_index(int ind);

        void set_surface_index(int ind);

        void setObjectIndex(int ind);

        void set_model_index(int index);

        void set_div_point_count_u(double value);
        void set_div_point_count_v(double value);

        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_scale(double new_scale);

        void set_scale_surface(double new_scale);

        void set_div_point_count(double value);

        void set_show_fod();
        void set_show_sod();
        void set_show_fod_cc();
        void set_show_sod_cc();
        void set_show_control_polygon_cc();
        void set_show_interpolating_cc();

        void set_show_shader();
        void set_shader_index(int ind);

        void set_derivate_1_vectors();
        void set_derivate_2_vectors();

        void set_show_control_polygon_patch();
        void set_show_interpolating_patch();
        void set_show_isoparametric_lines();

        void set_u_lines_count(int);
        void set_v_lines_count(int);

        void set_animation();

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);

        // curves

        void curve_insert_button_pressed(bool);
        void set_curve_index_1(int);
        void set_curve_direction_1(int);
        void set_curve_index_2(int);
        void set_curve_direction_2(int);
        void set_curve_shift_x(QString);
        void set_curve_shift_y(QString);
        void set_curve_shift_z(QString);
        void curve_shift_button_pressed(bool);
        void curve_continue_button_pressed(bool);
        void curve_merge_button_pressed(bool);
        void curve_join_button_pressed(bool);
        void curve_delete_button_pressed(bool);

    signals:

        void curve_insert_pressed(int);

    };
}
