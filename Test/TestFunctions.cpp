#include <cmath>
#include "TestFunctions.h"
#include "../Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI*2;
GLdouble spiral_on_cone::u_max = +TWO_PI*2;

DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

DCoordinate3 torus_knot::d0(GLdouble u)
{
    GLdouble c = cos(u*2/3.0);
    return DCoordinate3((2+c)*cos(u),(2+c)*sin(u),sin(u*2/3.0));
}

DCoordinate3 torus_knot::d1(GLdouble u)
{
    GLdouble u2 = 2*u/3, c = cos(u), s = sin(u), c2 = cos(u2), s2 = sin(u2);
    return DCoordinate3(-(c2+2)*s-2*s2*c/3,(c2+2)*c-2*s2*s/3,2*c2/3);
}

DCoordinate3 torus_knot::d2(GLdouble u)
{
    GLdouble u2 = 2*u/3.0, c = cos(u), s = sin(u), c2 = cos(u2), s2 = sin(u2);
    return DCoordinate3((12*s2*s+(-13*c2-18)*c)/9.0,-((13*c2+18)*s+12*s2*c)/9.0,-4*c2/9.0);
}

DCoordinate3 cylinder_spiral::d0(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c,s,u);
}

DCoordinate3 cylinder_spiral::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-s,c,1);
}

DCoordinate3 cylinder_spiral::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-c,-s,0);
}

DCoordinate3 lissajous::d0(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u), s2 = sin(5*u);
    return DCoordinate3(2*c,2*s,s2);
}

DCoordinate3 lissajous::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u), c2 = cos(5*u);
    return DCoordinate3(-2*s,2*c,5*c2);
}

DCoordinate3 lissajous::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u), c2 = cos(5*u);
    return DCoordinate3(-4*c,-4*s,25*c2);
}

DCoordinate3 vasque::d0(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u), c2 = cos(10*u);
    return DCoordinate3(2*c*c2,2*s*c2,c2*c2);
}

DCoordinate3 vasque::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u), c2 = cos(10*u), s2 = sin(10*u);
    return DCoordinate3(-2*(10*c*s2+s*c2), 2*c*c2-20*s*s2, -20*c2*s2);
}

DCoordinate3 vasque::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u), c2 = cos(10*u), s2 = sin(10*u);
    return DCoordinate3(40*s*s2-202*c*c2, -40*c*s2-202*s*c2,200*(s2*s2-c2*c2));
}


GLdouble torus::u_min = 0;
GLdouble torus::u_max = TWO_PI;
GLdouble torus::v_min = 0;
GLdouble torus::v_max = TWO_PI;
GLdouble torus::R     = 1;
GLdouble torus::r     = 0.5;

DCoordinate3 torus::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((R + r*sin(u)) * cos(v),
                        (R + r*sin(u)) * sin(v),
                        r*cos(u));
}
DCoordinate3 torus::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3( r * cos(u) * cos(v),
                         r * cos(u) * sin(v),
                         -r*sin(u));
}
DCoordinate3 torus::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(R + r*sin(u)) * sin(v),
                         (R + r*sin(u)) * cos(v), 0);
}

//================== Hyperboloid ==================
GLdouble hyperboloid::u_min = 0;
GLdouble hyperboloid::u_max = 3;
GLdouble hyperboloid::v_min = 0;
GLdouble hyperboloid::v_max = TWO_PI;

DCoordinate3 hyperboloid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((1 + cosh(u-3.0/2))*sin(v),
                        (1+ cosh(u-3.0/2))*cos(v),
                        sinh(u-3.0/2));
}
DCoordinate3 hyperboloid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(sinh(3.0/2-u)*(-sin(v)),
                        sinh(3.0/2-u)*(-cos(v)),
                        cosh(3.0/2-u));
}
DCoordinate3 hyperboloid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3((cosh(3.0/2-u)+1)*cos(v),
                        -(cosh(3.0/2-u)+1)*sin(v),
                        0);
}

//================== Cylindrical_helicoid ==================
GLdouble cylindrical_helicoid::u_min = 0;
GLdouble cylindrical_helicoid::u_max = 2;
GLdouble cylindrical_helicoid::v_min = 0;
GLdouble cylindrical_helicoid::v_max = TWO_PI;

DCoordinate3 cylindrical_helicoid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((2+u)*cos(v),
                        (2+u)*sin(v),
                        v);
}
DCoordinate3 cylindrical_helicoid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(v),
                        sin(v),
                        0);
}
DCoordinate3 cylindrical_helicoid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(u+2)*sin(v),
                        (u+2)*cos(v),
                        1);
}

//================== Alfred Gray ==================
GLdouble alfred_gray::u_min = 0;
GLdouble alfred_gray::u_max = 2;
GLdouble alfred_gray::v_min = 0;
GLdouble alfred_gray::v_max = TWO_PI;

DCoordinate3 alfred_gray::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(3*cos(u) + 1.0/2*(1+cos(2*u))*sin(v) - 1.0/2*sin(2*u)*sin(2*v),
                        3*sin(u) + 1.0/2*sin(2*u)*sin(u) - 1.0/2*(1-cos(2*u))*sin(2*v),
                        cos(u)*sin(2*u)+sin(u)*sin(v));
}
DCoordinate3 alfred_gray::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-sin(2*u)*sin(v)-cos(2*u)*sin(2*v)-3*u*sin(u)+3*cos(u),
                        -sin(2*u)*sin(2*v)+3*u*cos(u)+sin(u)*cos(u)*cos(u)+sin(u)*cos(2*u),
                        cos(u)*(3*cos(2*u)+sin(v)-1));
}
DCoordinate3 alfred_gray::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(u)*cos(u)*cos(v)-sin(2*u)*cos(2*v),
                        -2*sin(u)*sin(u)*cos(2*v),
                        sin(u)*cos(v));
}

//================== klein_bottle ==================
GLdouble klein_bottle::u_min = 0.0;
GLdouble klein_bottle::u_max = TWO_PI;
GLdouble klein_bottle::v_min = 0.0;
GLdouble klein_bottle::v_max = TWO_PI;

DCoordinate3 klein_bottle::d00(GLdouble u, GLdouble v)
{
        return DCoordinate3(3 * cos(u) + 1.0/2.0 * (1 + cos(2 * u)) * sin(v) - 1/2 * sin(2 * u) * sin(2 * v),
                                                3 * sin(u) + 1/2 * sin(2 * u) * sin(v) - 1.0/2.0 * (1 - cos(2 * u)) * sin(2 * v),
                                                cos(u) * sin(2 * v) + sin(u) * sin(v));
}

DCoordinate3 klein_bottle::d10(GLdouble u, GLdouble v)
{
        return DCoordinate3(-3 * sin(u) + (1/2 - sin(2 * u)) * sin(v) + cos(2 * u) * sin(2 * v),
                                                3 * cos(u) + cos(2 * u) * sin(v) -(1.0/2.0 + sin(2 * u)) * sin(2 * v),
                                                -sin(u) * sin(2 * v) + cos(u) * sin(v));
}

DCoordinate3 klein_bottle::d01(GLdouble u, GLdouble v)
{
        return DCoordinate3(3 * cos(u) + 1/2 * (1 + cos(2 * u)) * cos(v) - sin(2 * u) * cos(2 * v),
                                                3 * sin(u) + 1.0/2.0 * sin(2 * u) * cos(v) + (1 - cos(2 * u)) * cos(2 * v),
                                                2 * cos(u) * cos(2 * v) + sin(u) * cos(v));
}
